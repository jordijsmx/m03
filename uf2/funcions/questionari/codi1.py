def func(a, b, c):
    '''
    Funció que retorna una llista que depèn directament dels tres
    nombres introduïts com a arguments
    input: tres enters
    output: una llista
    '''


    if a in range(b,80,len(c)):
        return list(range(b,80,len(c))[0:3])
    else:
        return [a,b,c]

if __name__ == "__main__":
    print(func('5',12,45))    
