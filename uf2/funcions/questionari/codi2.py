#Definició de funcions
def is_pangram(frase):
    
    frase = frase.lower()
    abecedari = ["a","b","c","d","e","f","g","h","i","j","k","l","m",
    "n","o","p","q","r","s","t","u","v","w","x","y","z"]
    sum = 0
    for lletra in frase:
        llista = lletra_abecedari(lletra, abecedari, sum)
        sum = llista[0]
        abecedari = llista[1]
    
    return pangram(sum)
    
def lletra_abecedari(letter, abc, suma):
    
    if letter in abc:
        abc.remove(letter)
        suma += 1
    return [suma, abc]
    
def pangram(suma):
    if suma == 26:
        return True
    else:
        return False
    
#Codi principal
# frase = input("Introdueix una frase: ")
print(is_pangram("Whisky bueno: ¡excitad mi frágil pequeña vejez!"))
print(is_pangram("abcdefghijklmnopqrstuvwxyz"))