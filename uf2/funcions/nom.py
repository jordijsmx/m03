#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Programa amb funció que llegeix una cadena amb un nom i retorna les seves
#   inicials. 
#   Es pot utilitzar el mètode split
#   
# Especificacions d'entrada: 
#  Una cadena qualsevol
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		 nom
#   Execució 1    Jordi Jubete Sagredo   JJS

def inicials(nom):
    '''
    Funció que treu les inicials d'un nom
    Input: String
    Output: String
    '''
    espais = nom.split() # utilitza espai com a separador per defecte.
    inicials = ''
        
    for i in espais:
        inicials += i[0]
      
    if len(inicials) >= 3:
        no_majuscules = False
       
        
        if i[0] < chr(65) or i[0] > chr(90):
            no_majuscules = True

        if no_majuscules:
            return("Posa majúscules a les inicials.")

        print(inicials)
        return("ok")
    
    else:
        return("no ok")

nom_complet = input("Introdueix el teu nom complet: ")
realitzacio = inicials(nom_complet)
print(realitzacio)