#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Llegeix quants numeros es volen introduir, es llegeixen la quantitat introduida.
#       - Els nombres introduits s'afegeixen en una llista.
#       - Una funció anomenada sort_asc ordena la llista de números en ordre ascendent.
#
# Especificacions d'entrada: 
#  Un nombre INT per la quantitat de nombres a introduir i tants números com s'hagi escollit.
#
# Joc de proves:
#						Entrada 		Sortida
#                 		
#   Execució 1

# Funció que ordena els nombres de la llista

def not_sorted_list():
    
    quantitat = int(input("Introdueix quants números enters vols posar: "))
    llista = []
    i = 0

    while i < quantitat:
        num = int(input(f"Introdueix el número {i+1}: "))
        llista.append(num)
        i+=1
    return llista

def selection_sort(nums):



    return sorted(nums)

def mostrar(llista_ord):
    print("Els números introduïts, en ordre ascendent: ")
    for i in range(len(llista_ord)):
        if i == len(llista_ord) - 1:
            print(llista_ord[i])
        else:
            print(llista_ord[i],end=",")

llista = not_sorted_list()
llista_asc = selection_sort(llista)

mostrar(llista_asc)