def inicials(nom):
    '''
    Funció que treu les inicials d'un nom
    Input: String
    Output: String
    '''
    espais = nom.split() # utilitza espai com a separador per defecte.
    inicials = ''
        
    for i in espais:
        inicials += i[0]
      
    inicials = inicials.upper()
    return(inicials)

       
nom1 = input("Introdueix el primer nom: ")
nom2 = input("Introdueix el segon nom: ")

if inicials(nom1) == inicials(nom2):
    print(inicials(nom1))
else:
    print(False)

# També: print(incials(nom1 == inicials(nom2)))