#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Definim una funció per calcular els multiples de 2.
#
# Especificacions d'entrada: 
#  
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		
#   Execució 1

# Definim funció
def mult(valor):        # les variables d'una funció són LOCALS, és a dir,
    mult = False        # no cal utilitzar-les en variables externes (com abaix)

    if valor % 2 == 0:  # la funció NO s'executa, només si la cridem al codi principal
        mult = True
                        # input: un INT, output: booleà
    return mult

# Codi principal
n = int(input())

i = 2

while i < n:
    a = mult(i)  # la sortida de la funció es pot guardar a qualsevol variable

    if a:
        print(i)
    
    i += 1