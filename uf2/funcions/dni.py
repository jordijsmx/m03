#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Llegim un DNI i el programa retorna si és vàlid o no
#
# Especificacions d'entrada: 
#  
#  8 primers han de ser dígits              #   FUNCIÓ
#  la lletra correspongui als números       #  Input: el DNI
#  primer número NO pot ser un 0            #  Output: booleà 
#
# Joc de proves:
#						Entrada 		Sortida
#   
#   Execució 1


def lletra(char):

    lletra = 'TRWAGMYFPDXBNJZSQVHLCKE'
    res = int(char[:-1]) % len(lletra)
    lletra2 = lletra[res]


    if not char[-1] == lletra2:
        return False

    return True

def digit(num): # Podem utilitzar variables booleanes o returns per sortir
                # de la funció
    '''
    Funció que valida el format d'un DNI.
    Input: string
    Output: booleà
    '''
   
    if len(num) != 9:
        return False
    
    if not num[:-1].isdigit():
        return False

    if num[0] == "0":
        return False

    if num[8] < chr(65) or num[8] > chr(90): # No és necessari?
        return False
    
    if not lletra(num):
        return False
    
    return True

dni = input("Introdueix el teu DNI: ")

if lletra(dni):

    print("El teu DNI és vàlid.")

else:
    print("El teu DNI no és vàlid.")