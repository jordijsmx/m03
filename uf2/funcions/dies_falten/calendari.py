import datetime

def get_current_dt():
    '''
    Funcio que utilitza el mòdul datetime per obtenir la data actual.
    Input: INT
    Output: Bool
    '''
    avui = datetime.datetime.now()

    return avui # o return(datetime.datetime.now())

def iguals(aniv,dia,mes):
    '''
    Funció que comprova si l'aniversari introduït és el mateix que el dia i el mes actuals.
    Input: Cadena
    Output: Bool
    '''
    dia_aniv = aniv[0:2]
    mes_aniv = aniv[3:5]
    
    if int(dia_aniv) == dia and int(mes_aniv) == mes:
        return True

    return False

def augmentar_data(dia,mes,any): # el return d'aquesta funció serà [dia,mes,any]
    '''
    Funció que modifica la data segons si és l'ultim dia del mes o l'any.
    Input: STR
    Output: Llista 
    '''

    if ultim_dia_mes(dia,mes,any):
        if ultim_mes_any(mes):
            dia = 1
            mes = 1
            any+= 1
        
        else:
            dia = 1
            mes+= 1

    else:
        dia+= 1
    
    return [dia,mes,any]

def ultim_dia_mes(dia,mes,any):
    '''
    Funció que valida si és l'últim dia del mes i si l'any es de traspàs
    Input: INT per dia, STR per mes i INT o STR per any
    Output: Bool
    '''
 
    if mes in [4,6,9,11]:
        if dia == 30:
            return True
                
    elif mes == 2 and any_de_traspas(any):
        if dia == 29:
            return True
            
    elif mes == 2 and not any_de_traspas(any):
        if dia == 28:
            return True
         
    else:
        if dia == 31:
            return True

    return False

def ultim_mes_any(mes):
    '''
    Funció que valida si és l'ultim mes de l'any
    Input: INT
    Output: Bool
    '''
    return mes == 12

def any_de_traspas(any):
    '''
    Funció que valida si l'any és de traspàs
    Input: INT
    Output: Bool
    '''
    traspas = int(any) % 4 == 0 and (int(any) % 100 != 0 or int(any) % 400 == 0)

    return traspas

if __name__ == "__main__":
    print(augmentar_data(29, 2, 2012))
    print(ultim_dia_mes(29,2,2024))
    print(iguals('21/07','21','7'))
    print(any_de_traspas(2100))