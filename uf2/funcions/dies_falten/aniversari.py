import calendari

avui = calendari.get_current_dt()
dia_actual = avui.day
mes_actual = avui.month
any_actual = avui.year

aniversari = input("Introdueix el teu aniversari amb el format dd/mm: ")

dies = 0


while not calendari.iguals(aniversari,dia_actual,mes_actual):
        data = calendari.augmentar_data(dia_actual,mes_actual,any_actual)
        dia_actual = data[0]
        mes_actual = data[1]
        any_actual = data[2]
        dies+=1

print(dies)