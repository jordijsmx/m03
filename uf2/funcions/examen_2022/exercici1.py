
def comprimir_text(frase):
    '''
    Funció principal que crida a subfuncions
    Input: String
    Output: Booleà
    '''
    ant=''
    frase2=''
    cont=1

    for i in frase:
        if ant != i:
            if cont > 3:
                frase2=f"{frase2}@{cont}@{ant}"
            else:
                frase2+=cont*ant
            cont=1
        else:
            cont+=1
        ant = i

    return frase2

print(comprimir_text("Holaaaaaaaaaaaaaaaaaaaaaaaa commm essssssssstas?"))

def descomprimir_text(frase):
    '''
    Funció que descomprimeix el text a l'estat original
    Input: String
    Output: Bool
    '''
    frase2=''
    cont=0
    num=''
    for i in frase:
        if i == '@':
            cont+=1
        elif i.isdigit() and cont == 1:
            num+=i
        elif cont == 2:
            frase2+=int(num)*i
            cont=0
            num=''
        else:
            frase2+=i
    
    return frase2

print(descomprimir_text("Hol@24@a commm e@9@stas"))

'''
profe:

def comprimir(frase):
    frase_comp = ''

    i = -len(frase)

    while i < 0:
        cont = 1

        while frase[i] == frase[i+1] and i < -1:
            cont+=1
            i+=1
        
        frase_comp += subcadema_comp(i,cont,frase)
        i+=1

    return frase_comp

def subcadena_comp(i,cont,frase):
    if cont < 4:
        return frase[i]*cont
    else:
        return "@" + str(cont) + "@" + frase[i]

def subcadena_descomp(frase,i)
    subcadena=''

    if frase[i] == '@':
        subcadena+=frase[i]
    else:
        i+=1
        cont=''

        while frase[i] != '@':
            cont+=frase[i]
            i+=1
        
        cont = int(cont)
        i+=1
        subcadena+=frase[i]*cont

    i+=1  

    return [subcadena,i]
        
def descomprimir(frase):
    frase_descomp=''
    i=0

    while i < len(frase):
        returns_subcadena_descomp = subcadena_descomp(frase,i)

        frase_descomp += returns_subcadena_descomp[0]
        i= returns_subcadena_descomp[1]
    
    return frase_descomp

frase = input("Frase: ")

print(desconmprimir(frase))
'''

# prog 6 examen

llista = [1,2,3,4,5,6,7,8,9,10]

dif=1
i=0

while i < len(llista):
    print(llista[i])
    dif+=1
    i+=dif