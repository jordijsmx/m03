#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Demana a un usuari el seu nom, edat i si té dni.
#   - Retorna el nom, edat i dni en format llista, si ha indicat que té dni.
#   - Retorna el nom i l'edat en format llista, si ha indicat que no té dni.
#
# Especificacions d'entrada: 
#   1 nombre INT per l'edat, i 2 STRING, un per el nom i "SI" o "NO" per determinar
#   si té dni.
# 
#
# Joc de proves:
#						Entrada 		        Sortida
#                 nom   edat    te_dni     
#   Execució 1    Pau   23        NO            ['Pau','23']
#   Execució 2   Anna   28        SI,12345678A  ['Anna','28','12345678A']
#   Execució 3   Joan   18       hola           "Error: Sisplau, indica si tens dni."  
#


def gestionar_dni(nom,edat,te_dni):
    '''
    Funció que retorna el nom, edat i dni depenent de si ha indicat que té dni o no
    Input: 1 nombre INT per l'edat i 2 STRING pel nom i determinar si té dni.
    Output: 1 nombre INT per l'edat i 1 STRING pel nom si NO té dni.
            1 nombre INT per l'edat i 2 STRING pel nom i dni si té dni.
    ''' 
    if te_dni == 'SI':
        te_dni = input("Introdueix el teu dni: ")
        return mostrar_dades(nom,edat,te_dni)
    elif te_dni == 'NO':
        return mostrar_dades(nom,edat)

def mostrar_dades(nom,edat,te_dni=""):
    '''
    Funció que retorna una llista en funció de la resposta de l'usuari.
    Input:
    Output: 1 llista amb el nom i l'edat.
            1 llista amb el nom, l'edat i el dni.
    '''
    if te_dni == "":
        return [nom,edat]
    else:
        return [nom,edat,te_dni]

def sortida(llista):
    '''
    Funció que mostra un error si l'usuari no ha indicat correctament si té dni o no i mostra
    la sortida de la llista.
    Input:
    Output:
    '''
    if llista == None:
        print("Error: Sisplau, indica si tens dni.")
    else:
        print(llista)
    
# Codi principal

nom = input("Introdueix el teu nom: ")
edat = input("Introdueix la teva edat: ")
te_dni = input("Tens DNI? Respon amb SI/NO: ")

sortida(gestionar_dni(nom,edat,te_dni))

