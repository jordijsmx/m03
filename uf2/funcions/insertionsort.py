# Agafa el segon element de la llista i dir que es el "key"
# Comparem la clau amb l'element que hi ha abans
# Si la clau es més petit que aquest element, el convertim en el nombre anterior

# no podem utilitzar mètodes, com var.sort(), append...

def insertion_sort(llista):

    for i in range(1,len(llista)):
        key = llista[i]
        continuar = True
        a = i-1
        
        while a >= 0 and continuar:
            if key < llista[a]:
                llista[a+1] = llista[a]
                llista[a] = key
                a-=1

            else:
                continuar = False
    return llista

# Codi principal
llista = [3,7,8,1,4]

print(insertion_sort(llista))