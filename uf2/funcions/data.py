#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Programa que llegeix una data i valida el seu format  
#       dd/mm/aaaa
#           - separadors
#           - 10 elements
#           - any correcte
#           - mes correcte
#           - dia correcte
#
# Especificacions d'entrada: 
#   string
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		
#   Execució 1

# Funcio principal

def data_valida(data):
    '''
    Funció principal que valida la data completa.
    Input: String
    Output: Booleà
    '''
    if not separadors(data):
        return False
    
    if len(data) != 10:
        return False
   
    if not any_valid(data):
        return False
    
    if not mes_valid(data):
        return False
    
    if not dia_valid(data):
        return False

    return True

# Funcions de comprovacions

def separadors(format):
    '''
    Funció que separa la data en 3 dades i valida el seu format.
    Input: String
    Output: Booleà
    '''
    data = format.split("/")

    if len(data) != 3:
        return False

    return True

def any_valid(any):
    '''
    Funció que valida l'any.
    Input: String
    Output: Booleà
    '''
    any = any[6:]

    if any.isdigit():
        if len(any) == 4:
            if int(any) > 0 and int(any) < 10000:
                return True

    return False

def mes_valid(data):
    '''
    Funció que valida el mes.
    Input: String
    Output: Booleà
    '''
    mes = data[3:5]

    if mes.isdigit():
        if len(mes) == 2:
            if int(mes) > 0 and int(mes) < 13:
                return True
            
    return False

def dia_valid(data):
    '''
    Funció que valida el dia i el seu mes corresponent.
    Input: String
    Output: Booleà
    '''
    dia = data[0:2]
    mes = data[3:5]

    if dia.isdigit():
        if len(dia) == 2:
            if dia_corr_mes(dia,mes):
                return True
        
    return False
'''
            if mes in '04,06,09,11':
                if int(dia) > 0 and int(dia) < 31:
                    return True
                
            elif mes == '02':
                if int(dia) > 0 and int(dia) < 29:
                    return True
         
            else:
                if int(dia) > 0 and int(dia) < 32:
                    return True
'''              
 

def dia_corr_mes(dia,mes):

    if mes in '04,06,09,11':
        if int(dia) > 0 and int(dia) < 31:
            return True
                
    elif mes == '02':
        if int(dia) > 0 and int(dia) < 29:
            return True
            
    else:
        if int(dia) > 0 and int(dia) < 32:
            return True

    return False

# Codi principal

data_user = input("Introdueix una data amb format dd/mm/aaaa: ")

if data_valida(data_user):
    print("La data és vàlida.")

else:
    print("La data no és vàlida.")