# L'utilitzarem com a mòdul
import datetime

def get_current_dt():
	'''
	funció ens que retorna la data d'avui
	'''
	
	avui = datetime.datetime.now()

	return avui
	
# -------------------------------------------------------------

def anys_de_traspas(any_act):
	'''
	funció que ens diu si un any és de traspas o no
	'''

	if int(any_act) % 4 == 0 and not (int(any_act) % 100 == 0  and int(any_act) != 400 == 0):
		return True

	return False

'''
if __name__ == "__main__":
	print(anys_de_traspas('2024'))
	print(anys_de_traspas('2023'))
	print(anys_de_traspas('2000'))
	print(anys_de_traspas('1996'))
'''

# ------------------------------------------------------------

def ultim_mes_any(mes_act):
	'''
	funció que ens diu l'últim mes de l'any
	'''
	return mes_act == 12

# ------------------------------------------------------------

def ultim_dia_mes(dia_act, mes_act, any_act):
	'''
	funció que ens diu si és l'últim dia del mes
	'''
	
	if str(mes_act) in '01,03,05,07,08,10,12':
		
		if int(dia_act) == 31:
			
			return True
			
	elif str(mes_act) in '04,06,09,11':
		
		if int(dia_act) == 30:
			
			return True
			
	elif str(mes_act) in '02':
		
		if anys_de_traspas(any_act):
			
			if int(dia_act) == 29:
				
				return True
		
		else:
			
			if int(dia_act) == 28:
				
				return True
				
	return False
	
'''
if __name__ == "__main__":
	print(ultim_dia_mes('12','03','1994'))
	print(ultim_dia_mes('29','02','2000'))
	print(ultim_dia_mes('29','02','1994'))
	print(ultim_dia_mes('12','45','2000'))
	print(ultim_dia_mes('31','12','2023'))
'''

# -----------------------------------------------------

def iguals(aniversari, dia_act, mes_act):
	'''
	funció que ens diu si són el mateix que avui
	'''	
		
	dia = aniversari[0:2]
	mes = aniversari[3:5]	

	if (int(dia) == dia_act) and (int(mes) == mes_act):
		
		return True
		
	return False

'''
if __name__ == "__main__":
	print(iguals('12/03/2022','12','03'))
	print(iguals('21/03/2022','23','03'))
'''

# -------------------------------------------------------

def augmentar_data(dia_act, mes_act, any_act):
	
	dia_act = int(dia_act)
	mes_act = int(mes_act)
	any_act = int(any_act)
	
	if not (ultim_dia_mes(dia_act, mes_act, any_act)):
			
		dia_act += 1
		
	else:

		if ultim_mes_any(mes_act):
			
			dia_act = 1
			mes_act = 1
			any_act += 1
		
		else:
			dia_act = 1
			mes_act += 1
			
	return [dia_act, mes_act, any_act]

print(augmentar_data('08','03','2023'))
print(augmentar_data('31','03','2023'))