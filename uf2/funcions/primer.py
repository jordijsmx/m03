#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Definim una funció per calcular els multiples de 2.
#
# Especificacions d'entrada: 
#  
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		
#   Execució 1 

# definim funció

def primer(valor):
    # Docstring per el help
    '''
    Funció que classifica un nombre en primer o no-primer
    Input: INT
    Output: booleà
    '''
    primer = True
    i = 2
    while valor > i:
        if valor % i == 0:
            primer = False
        i += 1

    return primer
# codi principal

n = int(input())

num = 2

while n > num:
    
    a = primer(num)
  
    if a:
        print(num)
    num += 1