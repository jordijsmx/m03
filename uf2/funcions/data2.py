# Programa que valida una data
# ------------------------------------------------------

def data_format_valid(data):
	# funció que valida el format de la data
	# string
	# booleà (bool)
	
	# funció separador
	if not es_separador(data):
		return False
	
	# 10 elements
	if not len(data) == 10:
		return False
		
	# any correcte
	if not any_correcte(data):
		return False
		
	# mes correcte
	if not mes_correcte(data):
		return False
	
	# any correcte
	if not dia_correcte(data):
		return False
	
	return True
	
# ------------------------------------------------------

def es_separador(data):
	# funció que valida que hi hagin dos separadors
	# string
	# booleà (bool)

	if not data.count('/') == 2:
		return False
		
	return True

# ------------------------------------------------------

def any_correcte(data):
	# funció que valida el format de l'any correcte
	# string
	# bool
	
	anys = data.split('/')[2]
	
	if anys.isdigit() == True:
		
		if not len(anys) == 4:
			return False
			
			if not(int(anys) > 0 and int(anys) < 10000):
				return False
			
	return True
	
# -----------------------------------------------------

def mes_correcte(data):
	# funció que valida el format del mes correcte
	# string
	# bool
	
	mes = data.split('/')[1]
	# mes = data[3:5]
	
	if mes.isdigit() == True:
		
		if len(mes) == 2:	
			
			if int(mes) > 0 and int(mes) < 13:
				return True
			
	return False

# --------------------------------------------------

def dia_mes(dia, mes):
	# funció que valida el dia del mes
	# string
	# bool
	
	if mes in '04,06,09,11':
		if not(int(dia) >= 1 and int(dia) <= 30):
			return False
			
	if mes == '02':
		if not(int(dia) >= 1 and int(dia) <= 28):
			return False
	else:
		if not(int(dia) >= 1 and int(dia) <= 31):
			return False
			
	return True

# ---------------------------------------------------

def dia_correcte(data):
	# funció que valida el format del dia
	# string
	# bool
	
	dia = data.split('/')[0]
	mes = data.split('/')[1]
	
	if dia.isdigit() == True:
		
		if len(dia) == 2:
			return True
			
		if not dia_mes(dia,mes):
			return False
	
	return True


# Programa principal 
data = input("Introdueix una data ")

if data_format_valid(data):
	print('És una data correcte')

else:
	print('No és una data correcte')