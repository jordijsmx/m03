import sys

def digits(num, sum):

	if num >= 10:
		
		a = int(str(num)[0])
		b = int(str(num)[1:])
		
		if a == 1 and b == 0:
			sum += 1
		
		num -= 1
		return digits(num, sum)
		
	else:
		sum += 1
		return sum
		
numero = int(sys.argv[1])

print(digits(numero, 0))
