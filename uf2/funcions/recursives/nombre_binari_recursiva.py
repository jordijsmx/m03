# Funció recursiva

def decimal_binari(nombre):
	
	maxim = 2 ** nombre
	nombres_binaris= []
	
	for i in range(maxim):
	
		binari = ""

		while i != 0:
	
			nombre_binari = i % 2
			binari = binari + str(nombre_binari)
			i= i // 2
	
		posicio = 0
		binari_invertit = ""
		for i in binari:
	
			binari_invertit = binari_invertit + binari[posicio -1]
			posicio += 1

		
		if len(binari_invertit) < nombre:
			zeros = nombre - len(binari_invertit)
		
			binari_invertit = f"{zeros * '0'}{binari_invertit}"
		
		#print(binari_invertit)
	
	
		if nombre % 2 == 0:
			extrem = nombre // 2
			banda1 = binari_invertit[0:extrem]
			banda2 = binari_invertit[extrem:]
		
			suma1 = 0
		
			for i in range(len(banda1)):
			
				suma1 = suma1 + int(banda1[i])
			
			suma2 = 0
		
			for i in range(len(banda2)):
				suma2 = suma2 + int(banda2[i])
			
			if suma1 == suma2:
				nombres_binaris.append(binari_invertit)
			
			
		else:
		
			extrem = nombre // 2
			banda1 = binari_invertit[0:extrem]
			banda2 = binari_invertit[extrem+1:]
		
			suma1 = 0
		
			for i in range(len(banda1)):
			
				suma1 = suma1 + int(banda1[i])
			
			suma2 = 0
		
			for i in range(len(banda2)):
				suma2 = suma2 + int(banda2[i])
		
			if suma1 == suma2:
				nombres_binaris.append(binari_invertit)
			
	return(nombres_binaris)



# Programa principal
import sys
nombre = int(sys.argv[1])
print(decimal_binari(nombre))




