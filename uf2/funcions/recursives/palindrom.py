def es_palindromo(cadena):
    if len(cadena) <= 1:
        return True
    elif cadena[0] != cadena[-1]:
        return False
    else:
        return es_palindromo(cadena[1:-1])
    
print(es_palindromo("dabalearrozalazorraelabad"))

def verificar_palindrom(palindrom,part_final='',palindrom_complert=''):

    if part_final == '':
        palindrom_complert = palindrom


    if palindrom != '':
        part_final += palindrom[-1]
        nova_palindrom = palindrom[:-1]

        return verificar_palindrom(nova_palindrom,part_final,palindrom_complert)
    else:

        if palindrom_complert == part_final:
            print('És palindrom')
        else:
            print('No és un palindrom')


verificar_palindrom('dabalearrozalazorraelabad')
cadena = 'hola aloh'