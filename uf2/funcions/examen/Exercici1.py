#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete

import random

# Funcions
def randomCard ():
    '''
    Funció que retorna un nombre aleatori de l'1 al 12.
    Input: No és necessari
    Output: 1 nombre INT de l'1 al 12
    '''
    
    return random.randint(1,12)

def result(primera,segona,tercera):
    '''
    Funció que calcula la suma de la puntuació segons els nombres rebuts de la funció randomCard.
    Input: No és necessari
    Output: 1 Nombre FLOAT
    '''

    if primera in [8,9,10,11,12]:
        primera = 0.5
    
    if segona in [8,9,10,11,12]:
        segona = 0.5
    
    if tercera in [8,9,10,11,12]:
        tercera = 0.5

    resultat = primera + segona + tercera

    return resultat

def prize(diners,aposta,primera,segona,tercera):
    '''
    Funció que realitza els càlculs dels diners del jugador segons el resultat d'una partida.
    Input: 2 Nombres FLOAT
    Output: 1 nombre FLOAT
    '''
    resultat = result(primera,segona,tercera)

    if resultat < 7.5:
        diners = float(diners)
    elif resultat > 7.5:
        diners = float(diners)-float(aposta)
    else:
        diners = float(diners)+float(aposta)
    
    return diners

def main_func():
    '''
    Funció que simula una partida del joc de cartes
    '''
    print("7 i 1/2 MÍNIM")

    diners = int(input("Quants diners tens?: "))
    aposta = int(input("Quants diners vols apostar?: "))
    
    primera = randomCard()
    segona = randomCard()
    tercera = randomCard()
    
    print(f"Carta 1: {primera}")
    print(f"Carta 2: {segona}")
    print(f"Carta 3: {tercera}")

    if result(primera,segona,tercera) == 7.5:
        print("Partida guanyada!!!")
    else:
        print("Partida perduda...")
    
    print(f"T'has quedat amb {float(prize(diners,aposta,primera,segona,tercera))}€")
    
# Codi principal

jugues = input("Vols jugar?: ")

while jugues == "si":
    main_func()
    jugues = input("Vols jugar?: ")