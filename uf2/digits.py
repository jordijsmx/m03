from sys import argv as argument

def digits(num,suma):
    #if num >= 10:
        num1 = int(str(num)[0])
        num2 = int(str(num)[1:])

        if num1 == 1 and num2 == 0:
            suma+=1
        
        num-=1
        # return digits(num,suma) # si no volem recursio, no cridem a la funció
        return [num,suma]
    #else:
        #suma+=1
        # return suma
        

# Codi Principal
'''
num = int(argument[1])

print(digits(num,0))
'''
# Sense recursio
def digits_total(num,sum):
    while num >= 10:
        resposta = digits(num,sum)
        num = resposta[0]
        sum = resposta[1]
        print(num)
    return sum+1

numero = int(argument[1])

print(digits_total(numero,0))