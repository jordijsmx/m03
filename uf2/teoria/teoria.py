# fitxer modul.py
def parlar():
    print("Hola món!!")
print(__name__)


# Pel simple fet de fer "import modul" s'executa el codi de dins a
# l'executar el fitxer, encara que no es cridi al modul

# fitxer prncp.py
import modul  
arg = input()
modul.parlar(arg)   # s'executa el contingut del modul al executar
                    # el fitxer "python3 prncp.py"

# Quan executem un programa desde ell mateix, el nom intern no és
# el nom de l'script, si nò que es "main"

# fitxer modul.py
# aixo donara True només si executem "python3 modul.py"
if __name__ == "__main__":
    frase = input("Què vols que digui? --")
    print(frase)
    parlar() # per cridar a la funció

