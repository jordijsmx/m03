#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#       Programa principal que, utilitzant mòduls realitza:
#       - A l'execució l'usuari indicarà les dimensions de la creació de la matriu.
#       - Després li demanarà la introducció dels valors d'aquesta matriu.
#       - Els mòduls s'encarregaran de seccionar la matriu en capes.
#       - Després realitzarà la suma dels delements de la capa exterior i després la eliminarà.
#       - Finalment, mostrarà per pantalla el resultat de la suma dels valors de les seves capes.
#
# Especificacions d'entrada: 
#      - Un nombre INT a l'execucio del programa per indicar les dimensions de la matriu
#      - Un cop executat, tants nombres INT com valors tindrà aquesta matriu
#
# Joc de proves:
#						Entrada 		Sortida
#                 		
#   Execució 1   capes_matriu.py 2	  1           2         
#                                     
#                                     3           4
#
#                                     La matriu té 1 capa
#
#                                     La suma dels elements de la capa 0 és 10

# Importació dels mòduls necessaris
from sys import argv as argument
import gestio_matriu as gestio
import desenvolupament_matriu as creacio

# Executem el programa amb el primer argument donat per l'usuari
matriu = creacio.crear_matriu(argument[1])

# Utilitzem el mòdul de creació per crear i visualitzar la matriu
creacio.visualitzar_matriu(matriu)

# Utilitzem el mòdul de gestió per realitzar les instruccions demanades
print(f"La matriu té {gestio.num_capes(argument[1])} capes")
print(gestio.suma_exterior(matriu))
print(gestio.reduir_matriu(matriu))