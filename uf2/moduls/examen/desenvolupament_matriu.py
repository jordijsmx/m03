def crear_matriu(num):
    '''
    Funció que crea una matriu quadrada de N x N dimensions.
    Input: un nombre INT per a les dimensions.
    Output: Llista de nombres INT com a matriu.
    '''

    fil = int(num)
    col = int(num)

    matriu = []

    for i in range(fil):
        b = []
        for j in range(col):
            num = int(input(f"Introdueix l'element {i} {j}: "))
            b.append(num)
         
        matriu.append(b)
    
    return matriu

def visualitzar_matriu(matriu):
    '''
    Funció que, donada una matriu, la mostra per pantalla.
    Input: Llista a dins de una llista de nombres INT com a matriu.
    Output: Mostra la matriu
    '''
    for i in range(len(matriu)):
        a = ""
        for j in range(len(matriu)):
            if j != len(matriu)-1:
                a+=f"{matriu[i][j]:^8}"
                a+="|"
            else:
                a+=f"{matriu[i][j]:^8}"

            
        print(f"\n",a,"\n")
        
        if i != len(matriu)-1:
            print(f"_"*9*len(matriu))

if __name__ == "__main__":


    visualitzar_matriu(crear_matriu(3))