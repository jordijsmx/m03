

def reduir_matriu(matriu):
    '''
    Funció que, donada una matriu, retorna la mateixa pero sense la capa exterior original
    Input:
    Output:
    '''
    matriu = []

    for i in range(1,len(matriu)-1):
        fila = []
        for a in range(1,len(matriu)-1):
            fila.append(matriu[i][a])
        matriu.append(fila)
            
    return matriu

def suma_exterior(matriu):
    '''
    Funció que, donada una matriu, retorna la suma dels elements de la capa exterior.
    Input:
    Output:
    '''
    suma = 0

    for fila in range(len(matriu)):
        if fila == 0 or fila == len(matriu)-1:
            for num in matriu[fila]:
                suma+=num
                
        else:
            suma+=matriu[fila][0]
            suma+=matriu[fila][-1]

    return suma    

def num_capes(matriu):
    '''
    Funció que, donada una matriu, retorna quantes capes té.
    Input:
    Output:
    '''
    n = len(matriu)
    
    if n % 2 != 0:
        n+=1
    n = n // 2

    return n
      
if __name__ == "__main__":
    num_capes([1,2,3,4])
    print(suma_exterior([[1,2,3],[4,5,6],[7,8,9]]))