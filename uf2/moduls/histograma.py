#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#  
#
# Especificacions d'entrada: 
#  
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		
#   Execució 1
from sys import argv as argument# crea una llista amb els arguments O import sys

def comptar(frase):
    
    comptar = []

    for i in frase:
        comptar.append(len(i))

    comptar.sort()
    return comptar

def mostrar(llista,char):

    max = llista[-1]
    i=1
    
    while i <= max:
        print(i,':', llista.count(i)*char)
        i+=1


# Codi principal
'''
frase = input("Frase: ")
char = input("Caràcter: ")

frase = argument[1] # agafem la llista creada per sys, 0 és el programa en si, histograma.py
char = argument[2]
'''
frase = argument[1:-1]
char = argument[-1]

mostrar(comptar(frase),char)

