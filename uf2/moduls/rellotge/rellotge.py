#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Mòdul del rellotge

def temps(hora,min,seg):
    hora = hora * 3600
    min = min * 60

    total = hora + min + seg

    return total