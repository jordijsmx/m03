#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Mòdul del rellotge

from sys import argv as argument
from rellotge import temps as segons

hora = argument[1]
min = argument[2]
seg = argument[3]
print(segons(int(hora),int(min),int(seg)))