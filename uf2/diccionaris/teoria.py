'''
diccionari
-----------
Conjunt de dades sense ordenar, cridant-lo amb slicing no funciona, ja que
no té cap ordre.

Els diccionaris estan formats per parells de valors.

Exemples:

        - key= 12345678A
Alumne 1
        - value= Pau

        - key= 121233178B
Alumne 2
        - value= Anna
    

       -key= 1234 ABC 
Cotxe 1
        -value= ford fiesta

A "key" i "value" se li diuen "objectes". No podem fer objectes amb el mateix key,
sobreescriuria el valor antic. Es poden tenir tants objectes com volguem posar.

En un diccionari no poden haver 2 valors iguals        

https://www.w3schools.com/python/python_dictionaries.asp
'''
# Exemple de diccionari
# El tipus de dada és DICCIONARI

dcc = {
    "1234ABC":["ford","groc"], # també podem fer una llista a dins del diccionari
    "2370FAG":["toyota","gris"]
    
}

dcc["7081BCC"] = ["ferrari","vermell"]        # Afegir o modificar

dcc.update({"7081BCC":["renault","negre"]})   # Afegir o modificar

for i in dcc:
    print(i)  # agafa els keys del diccionaris, amb aquesta key ja sabem a quin valor estem apuntant
    print(dcc[i])  # accedim als valors de l'identificador "i"


a = dcc["1234ABC"]  # guardem els VALORS d'aquesta key, es a dir, ["ford","groc"]
print(a)

a = list(dcc.keys()) # mostrar les claus, transformant-les a llista amb list()
print(list(dcc.values())) # mostrar els valors

print("7081BCC" in a) # retorna TRUE

print(dcc.items()) # Ho mostra tot, keys amb els seus values