# Modul que utilitza el programa "alumnes.py" per a introduir alumnes a un diccionari


def create_dic(num): # demana nombre d'alumnes i introduim DNI, nom i cognom. Retorna diccionari amb les dades
    '''
    Demana nombre d'alumnes i introduim DNI, nom i cognom.
    Input: 
    Output:
    '''
    dcc = {}

    for i in range(num):
        dni = input("Introdueix el DNI: ")
        nom = input("Introdueix el nom: ")
        cognom = input("Introdueix el cognom: ")

        dcc.update({dni:[nom,cognom]})

        # dcc[dni]=[nom,cognom]

        return dcc

def get_alumnes(diccionari):  # fem print del diccionari
    for i in diccionari:
        print(f"{diccionari[i][0]},{diccionari[i][1]},dni:{i}")


def update_alumne(diccionari,update): # a update posarem una d'aquestes: "dni","nom","cognom"
    
    dni = input("Introdueix el DNI de l'alumne que vols actualitzar: ")

    for i in diccionari:
        if i == dni:
            if update == "nom":
                nou_nom = input(f"Introdueix el nou nom: ")
            elif update == "cognom":
                nou_cog = input(f"Introdueix el nou cognom: ")
                diccionari.update({dni:[diccionari[i][0],nou_cog]})
            elif update == "dni":
                nou_dni = input(f"Introdueix el nou DNI: ")
                
        
    return diccionari


# Codi principal

dcc = {'12345678A': ['jordi', 'perez'], '52345678A':['manel', 'garcia'],'62345678A': ['joan-ramon', 'vila']}

if __name__ == "__main__":
    #get_alumnes(create_dic(2))

    update_alumne(dcc,"cognom")