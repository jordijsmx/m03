#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete487
#
# Descripció: 
#
# Especificacions d'entrada: 
#
# 
#
# Joc de proves:
#						Entrada 		Sortida
#		Execució 1         				
#						  			 
#		Execució 2		   			 
#

TAULES = 17

convidats = int(input())

taules = convidats // TAULES

taules_sobres = convidats % TAULES

print(taules,"taules de",TAULES,"i una taula de",taules_sobres,"persones.")
