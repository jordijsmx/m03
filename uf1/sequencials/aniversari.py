# Definim les constants de la data actual
DIA = 25
MES = 10
ANY = 2022

# Variables d'entrada de la data del següent aniversari
dia_aniv = int(input())
mes_aniv = int(input())
any_aniv = int(input())

# Passem a dies la data actual com la data del següent aniversari
dies_actuals = DIA + (MES * 30) + (ANY * 365)
dies_aniv = dia_aniv + (mes_aniv * 30) + (any_aniv * 365)

# Restem el resultat
dies_falten = dies_aniv - dies_actuals

# Mostrem la quantitat de dies que falten
print(dies_falten)