# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-23
# 
# Autor: Pau Besses
# Data: 25/10/2022
#
# Versió: 1
#
# Descripció: Programa que llegeix els valors d'una hora, un minut i un
# segon, i mostra els valors corresponents al segon següent
#
# Especificacions d'entrada: 
# Tres nombres enters, majors o iguals a 0. Dos
# d'ells, més petits que 60; i l'altre, més petit que 24. 
#
# Joc de proves:
#
#		Entrades						Sortides
#		h		m		s			h_fin			m_fin		s_fin
#				
# Execució 1    12		45		3			12			45			4
# Execució 2    1		23		59			1			24			0
# Execució 3    0		59		58			0			59			59
# Execució 4    7		59		59			8			0			0
# Execució 5    23		59		59			0			0			0
#
# Codi:

h = int(input("Introdueix l'hora: "))
m = int(input("Introdueix el minut: "))
s = int(input("Introdueix el segon: "))

s = s + 1
s_mod = s // 60
s_rsd = s % 60
s_fin = s_rsd

m = m + s_mod
m_mod = m // 60
m_rsd = m % 60
m_fin = m_rsd

h_fin = h + m_mod
h_fin = h_fin % 24

print(h_fin,m_fin,s_fin)
