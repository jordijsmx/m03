#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# 
# Fes un programa que normalitzi el temps a partir de la lectura 
# d'unes hores, minuts i segons.
#
# Especificacions d'entrada: 
#
# 3 nombres enters >= 0
# 
#
# Joc de proves:
#						Entrada 		Sortida
#		Execució 1      3 118 195  		 5 1 15	
#						  			 
#		Execució 2		0  0  0		     0 0 0

#       Execució 2		1  2  3		     1 2 3


# Llegim entrades

hores = int(input())
minuts = int(input())
segons = int(input())

# Passar a segons

seg = hores * 3600 + minuts * 60 + segons

# Normalitzar segons

hores = seg // 3600
minuts = seg // 60 % 60
segons = seg % 60

# Mostrar

print(hores, minuts, segons)