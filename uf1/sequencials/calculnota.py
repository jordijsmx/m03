#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció:  
# Fes el joc de proves i E.E. d'un programa que efectui el càlcul
# de la nota final a partir de tres exàmens. La nota final serà la mitjana si les
# faltes d’assistència son menors que el 25% o un 0 si son superiors.
#
# Especificacions d'entrada: 
# 3 notes float entre 0 o 10 i un d'assitències int de 0 a 100.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#		Execució 1      10 9 8 10  		   9	
#						  			 
#		Execució 2		6 7 8  26   	   0
# 
#       Execució 3      9 3	7  15
# 
#       Execució 4      3 4 6  25          0
# 
#           5           0 0 0   5          0   


# Tres notes float entre 0 o 10 i un d'assitències int de 0 a 100.
nota1 = float(input())
nota2 = float(input())
nota3 = float(input())

# Un nombre int igual a 0 o igual a 1 (nombre binari d'un digit)

faltes = int(input())


mitjana = ((nota1 + nota2 + nota3) / 3) * faltes

print(mitjana)

