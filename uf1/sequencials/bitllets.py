
# Entrada de diners

diners = int(input())

# Càlcul del número de bitllets

bit500 = diners // 500
bit200 = diners % 500 // 200
bit100 = diners % 500 % 200 // 100
bit50 = diners % 500 % 200 % 100 // 50
bit20 = diners % 500 % 200 % 100 % 50 // 20
bit10 = diners % 500 % 200 % 100 % 50 % 20 // 10
bit5 = diners % 500 % 200 % 100 % 50 % 20 % 10 // 5

# Mostrar quantitat de bitllets

print(bit500,"bitllets de 500.")
print(bit200,"bitllets de 200.")
print(bit100,"bitllets de 100.")
print(bit50,"bitllets de 50.")
print(bit20,"bitllets de 20.")
print(bit10,"bitllets de 10.")
print(bit5,"bitllets de 5.")
