#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#
# Especificacions d'entrada: 
# Un nombre INT > 0.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#		Execució 1        735	     2 anys, 0 mesos, 5 dies			
#						  			 
#		Execució 2		  70		 0 anys, 2 mesos, 10 dies	 
#

dies = int(input())

anys_totals = dies // 365
dies_restants = dies % 365
mesos = dies_restants // 30
dies = dies_restants % 30

print(anys_totals,"anys",mesos,"mesos",dies,"dies")