#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# 
# Rellotge
#
# Especificacions d'entrada: 
#
# 3 nombres enters
# 
#
# Joc de proves:
#			Entrada 		Sortida
#	1	     1 2 3  		 1 2 4	
#						  			 
#   2		23 59 59		 0 0 0
#   
#   3       13 59 0          13 59 1

 
hores = int(input())
minuts = int(input())
segons = int(input())

segons = segons + 1
s_restants = segons % 60
minuts = minuts + segons // 60
m_restants = minuts % 60
hores_finals = (hores + minuts // 60) % 24

print(hores_finals, m_restants, s_restants)