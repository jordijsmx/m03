#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# Escriu un programa que llegeixi una mesura de temps expressada en hores, minuts i
# segons amb valors arbitraris i que la transformi en una expressió correcta. 
#  Per exemple, si tenim la mesura 3h, 118m i 195s ens retorni com a resultat 5h 1m 15s
#
# Especificacions d'entrada: 
#
#  
#
# Joc de proves:
#						Entrada 		Sortida
#		Execució 1         				
#						  			 
#		Execució 2		   			 
#


hores = int(input())
minuts = int(input())
segons = int(input())

segons_finals = segons % 60
minuts_sumats = segons // 60 + minuts
minuts_finals = minuts_sumats % 60
hores_finals = minuts_sumats // 60 + hores

print(hores_finals, minuts_finals,segons_finals)

