#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# 
# Programa que normalitza el temps a partir d'unes hores, minuts i segons.
# Mostra el segon següent
#
# Especificacions d'entrada: 
#
# 3 nombres enters
# 
#
# Joc de proves:
#			Entrada 		Sortida
#	1	     1 2 3  		 1 2 4	
#						  			 
#   2		23 59 59		 0 0 0
#   
#   3       13 59 0          13 59 1

 
hores = int(input())  
minuts = int(input())  
segons = int(input()) 

segons = segons + 1
segons_totals = segons % 60
minuts_restants = segons // 60 + minuts
minuts_totals = minuts_restants % 60
hores_totals = (minuts_restants // 60 + hores) % 24

print(hores_totals,minuts_totals,segons_totals)