#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2020-21
# 
# Autor: Pau Besses
# Data: 19/10/2022
#
# Versió: 1
#
# Descripció:  Programa que llegeix el nombre de convidats i diu les taules que
# necessitaré i de quantes persones és la 'taula diferent', tenint en compte que a cada taula 
# hi van 12 persones, menys en la taula diferent que hi van les persones que sobrin
#
# Especificacions d'entrada: 
#
# Un nombre enter major o igual a 0
#
# Joc de proves:
#						Entrada 		Sortida
#						convidats		taules_totals		taula_diferent
#		Execució 1         		314			27			2
#												
#		Execució 2		  	4			1			4
#										
#		Execució 3			36			3			0
#											
#
# Codi:

convidats = int(input("Introdueix el nombre de convidats "))

num_taules_de_12_persones = convidats // 12
taula_diferent = convidats % 12

existeix_taula_diferent = (taula_diferent*3)//(taula_diferent*3-1)
taules_totals = num_taules_de_12_persones + (existeix_taula_diferent)

print(taules_totals, taula_diferent)


print("Necessitaràs " + str(num_taules_de_12_persones) + " taules de 12 persones i una taula de " + str(taula_diferent) + " persones")







