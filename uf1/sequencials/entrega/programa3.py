#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Llegeix dos valors com a llargada dels radis de dos cercles.
#   Retorna el valor d'un tercer radi que el cercle d'aquest
#   tingui una àrea igual a la mitjana dels dos radis anteriors.
#
# Especificacions d'entrada: 
#   Dos nombres float qualsevols positius.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 	radi1   radi2        radi3	
#   Execució 1    	  2		  2			  2.0 			 
#	Execució 2	    28.4    23.33        25.98892937386994
# 	Execució 3	     3.5      7          5.533985905294664

# Introduim els nombres dels radis dels dos cercles:
radi1 = float(input("Introdueix la longitud del primer radi: "))
radi2 = float(input("Introdueix la longitud del segon radi: "))

# Apliquem la fòrmula per a obtenir l’àrea dels dos cercles:
area1 = (3.14 * radi1 **2)
area2 = (3.14 * radi2 **2)

# Calculem l’àrea mitjana dels dos cercles anteriors:
mitja_area = (area1 + area2) / 2

# Aillem el radi de la formula per trobar-lo.
radi3 = (mitja_area / 3.14) **(1/2)

# Mostrem el resultat de l’àrea del tercer radi:
print("La longitud del tercer radi és",radi3)