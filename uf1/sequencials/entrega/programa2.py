#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# Llegeix el radi de dos cercles i mostra per patanlla un 1
# si les àrees són iguals i un 0 si no ho són.
#
# Especificacions d'entrada: 
# 2 nombres float positius.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 	radi1   radi2       resultat
#   Execució 1    	  7		  7            1				  			 
#	Execució 2	     25.60    9            0  
# 	Execució 3       11.0   11.0           1  
#	Execució 4        222    111           0  

radi1 = float(input("Introdueix el primer radi: "))
radi2 = float(input("Introdueix el segon radi: "))

resultat = ((3.14 * radi1 **2) // (3.14 * radi2**2)) * ((3.14 * radi2 **2) // (3.14 * radi1**2))

print(int(resultat))