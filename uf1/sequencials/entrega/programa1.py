#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# Llegeix un nombre real positiu i mostra per pantalla el nombre enter obitngut 
# realitzant el truncament superior del nombre llegit.
#
# Especificacions d'entrada: 
# Un nombre float >= 0
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		   n               
#   Execució 1    	     3.12			   4				  			 
#	Execució 2	         3.99              4
# 	Execució 3	           4               4

n = float(input("Introdueix un número positiu: "))

m = n + 1 - (int(n)//n);

print(int(m))