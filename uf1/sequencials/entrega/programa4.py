#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Llegeix un nombre de 8 dígits d'una direcció binària i
#   la converteix al seu valor decimal.
#
# Especificacions d'entrada: 
#   Un nombre de 8 digits de 0 i 1.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		
#   Execució 1    		11111111          255					  			 
#	Execució 2	        00000001           1
# 	Execució 3	        10101101          173

# Introduïm el nombre binari de 8 dígits:
num = int(input("Introdueix una direcció binària de 8 digits: "))

# Dividim el nombre entre el nombre 1 seguit de tants 0 com la posició de la potència
# corresponent al dígit, per així extreure'l, en aquest cas, 7:
num1 = num // 10000000

# Guardem la resta d'aquesta divisió en una variable, per
# utilitzar aquest nombre en la divisió del següent dígit:
num_resta = num % 10000000

# Utilitzem el nombre extret anteriorment per calcular
# la potència corresponent al seu dígit:
num1 = num1 * 2**7

# Ara utilitzem la resta de la primera divisió per extreure-li
# el següent dígit:
num2 = num_resta // 1000000

# Continuem aplicant el mateix mètode utilitzat per
# al primer dígit, fins a l'ultim:
num_resta = num_resta % 1000000
num2 = num2 * 2**6

num3 = num_resta // 100000
num_resta = num_resta % 100000
num3 = num3 * 2**5

num4 = num_resta // 10000
num_resta = num_resta % 10000
num4 = num4 * 2**4

num5 = num_resta // 1000
num_resta = num_resta % 1000
num5 = num5 * 2**3

num6 = num_resta // 100
num_resta = num_resta % 100
num6 = num6 * 2**2

num7 = num_resta // 10
num_resta = num_resta % 10
num7 = num7 * 2**1

num8 = num_resta // 1
num_resta = num_resta % 1
num8 = num8 * 2**0

# Un cop extrets tots els dígits i aplicats el seus càlculs
# de les potències corresponents, sumem tots els digits entre
# sí per obtenir el nombre decimal:
decimal = num1 + num2 + num3 + num4 + num5 + num6 + num7 + num8

# Mostrem la variable del nombre binari passat a decimal:
print(num,"és equivalent a",decimal,"en decimal.")