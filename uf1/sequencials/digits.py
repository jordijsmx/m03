# Llegeix un nombre natural de 5 digits, la sortida sera cada 
# digit del nombre per separat.

num = int(input())

num1 = num // 10000
num_resta = num % 10000

num2 = num_resta // 1000
num_resta = num % 1000 

num3 = num_resta // 100
num_resta = num % 100

num4 = num_resta // 10
num_resta = num % 10

num5 = num_resta // 1

print('',num1,'\n',num2,'\n',num3,'\n',num4,'\n',num5)

'''
num = input()

print('',num[0],'\n',
         num[1],'\n',
         num[2],'\n',
         num[3],'\n',
         num[4])
'''