
# Llegir els bits

binari1 = int(input())
binari2 = int(input())
binari3 = int(input())
binari4 = int(input())
binari5 = int(input())
binari6 = int(input())
binari7 = int(input())
binari8 = int(input())

# Calcular

binari1_dec = binari1 * 2**7
binari2_dec = binari2 * 2**6
binari3_dec = binari3 * 2**5
binari4_dec = binari4 * 2**4
binari5_dec = binari5 * 2**3
binari6_dec = binari6 * 2**2
binari7_dec = binari7 * 2**1
binari8_dec = binari8 * 2**0

# Sumar els decimals

decimal = binari1_dec + binari2_dec + binari3_dec + binari4_dec + binari5_dec + binari6_dec + binari7_dec + binari8_dec

# Mostrar

print(decimal)