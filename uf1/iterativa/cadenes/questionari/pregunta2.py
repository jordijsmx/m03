'''
Un matemàtic s'inventa l'operació "producte invertit de 2 vectors", definida de la següent manera:

Donats dos vectors v1 i v2 de dimensió n, el seu producte invertit és la suma dels següents productes:
     - el 1er terme de v1 multiplicat pel terme n de v2
     - el 2on terme de v1  multiplicat pel terme n-1 de v2
     - ...
     - el terme n de v1 multiplicat pel 1er terme de v2

Per exemple:
   - Si v1 = {1, 2, 3} i v2 = {4, 5, 6}, el seu producte invertit és 1 * 6 + 2 * 5 + 3 * 4 = 28
   - Si v1 = {1, 2, 3, 4, 5} i v2 = {6, 7, 8, 9, 10}, el seu producte invertit és 1 * 10 + 2 * 9 + 3 * 8 + 4 * 7 + 5 * 6 = 110 

Tenim el següent codi, que és l'estructura del programa que permet calcular el producte invertit de dos vectors:
'''
 

#creació dels vectors

vector1 = [1,2,3]

vector2 = [4,5,6]

 
#codi

suma = 0

for i in range(len(vector1) -1, -1, -1):

#Línia de codi a desenvolupar
   
   suma += vector1[-i-1] * vector2[i]

print(suma)

