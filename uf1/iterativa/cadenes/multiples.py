#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Rep un número enter i informa de si és múltiple de 2,3 i 10.
#
# Especificacions d'entrada:
#   Nombre INT qualsevol.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		
#   Execució 1  


# Versió profe

num = input()

mult_dos = False
mult_tres = False
mult_deu = False

# Comprovem si és múltplie de 2:
if num[-1] in '02468':
    mult_dos = True
    
# Comprovem si és múltiple de 3:
n = num

while len(n) > 1:
    sum = 0
    for i in n:
        sum += int(i)

    n = str(sum)

mult_tres = n in '369'

# Comprovem si és múltiple de 10:
if num[-1] == '0':
    mult_deu = True

print("\n",num,"és múltiple de dos:\n",mult_dos,"\n",
      num,"és múltiple de tres:\n",mult_tres,"\n",
      num,"és múltiple de deu:\n",mult_deu)