#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#  Resoldre la lletra del DNI
#
# Especificacions d'entrada: 
#  Un nombre INT de 8 dígits
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		
#   Execució 1    									  			 
#	Execució 2	
# 	Execució 3	  

dni = int(input("Introdueix el teu DNI: "))

lletra = 'TRWAGMYFPDXBNJZSQVHLCKE'

res = dni % 23

print("La teva lletra és:",lletra[res])