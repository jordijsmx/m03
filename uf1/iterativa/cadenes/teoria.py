# TEORIA CADENES
'''
print('"Hola"')
print("H\thola")  # Tabulació
print("Salt de línia\n")
print("Salt de línia?\\n") # Escapar caràcters
print("El Jaume diu \"Hola\"")
print('   Hola   ') # Espais
print(ord('Ç')) # Número del caràcter
print(chr(199)) # Printa el caràcter corresponent

a = ("Hola")

print(a[2]) # Mostra "l", comença per 0
print(a[-1]) # Mostra "a", comença pel final
print(a[1:]) # Del caràcter "o" fins al final
print(a[:-1]) # Tots menys l'últim caràcter
print(a[:1]) # Mostra "H
print(a[1:3]) # Mostra de "o" fins "l", EXCLOU el 3

# Recórrer posicions

for i in [1,8]:
    print(i)

for i in range(1,8):
    print(i)

for i in "Hola":
    print(i)
    
a = "Hola"
for i in range(0,len(a)):
    print(a[i])

# help(print) # Informació sobre funcions, substituir print per int, str...
'''
# F STRINGS
'''
frase1 = f"El nombre {8:12.2f} no és pi."

print(frase1)

for i in [1, 1456, 3 ,10, 8, 500043]:
    mult = i * 30.2 ** (1/2)
    print(f"{mult:.2f} correspon a {i} €")
'''
# STRING METHODS
'''
a = "    HOLA    "
a = a.center(15)
print(a)

b = a.strip()
print(b)

frase = "   Holamón com  estàs ?"
frase_llista = frase.split("#") # Si no posem res elimina els espais

print(frase_llista)
'''