#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#  Programa que compta la longitud d'una cadena
#
# Especificacions d'entrada: 
#  Cadena qualsevol
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		cadena
#   Execució 1    		 hola  			   4				  			 

cadena = input()

long = 0

for i in cadena:
    long += 1

print(long)