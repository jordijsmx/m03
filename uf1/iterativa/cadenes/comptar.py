#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Compta el número de paraules que hi ha en una cadena amb espais.
#
# Especificacions d'entrada: 
#   Una cadena qualsevol.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		cadena
#   Execució 1        hola que tal			3	
#   Execució 2   Hola Adeu     comestas     3

frase = input("Introdueix una frase: ")

sum = 0

for i in range(1,len(frase) ):
    if frase[i] == ' ' and frase[i-1] != ' ':
        sum += 1
    
sum += 1

if frase == '':
    sum = 0

print(sum)

'''
# Introduïm la frase

frase = input("Introdueix una oració: ")

# Eliminem els possibles espais abans de la frase

while frase[0] == " ":

  frase = frase[1:]

# Eliminem els possibles espais després de la frase

while frase[-1] == " ":

  frase = frase[:-1]

# Contem les seqüències lletra+espai que apareixen a la frase
# El nombre de paraules serà igual al nombre de seqüències + 1



if len(frase) == 1:
  
  print("Num paraules: 1")

else: 

  lletra_espai = 0

  for i in range(1,len(frase)):

    if frase[i] == " " and frase[i-1] != " ":

      lletra_espai += 1
  
  paraules = lletra_espai +1 

  print("Num paraules:", paraules)
'''