# Una llista és un conjunt d'elements
'''
a = ["hola","3"]
print(len(a))
print(len(a[0]))

b = ["adéu"]
b*=4
print(b)
'''
# Donada una llista busca el primer nombre parell
'''
llista = [1,5,3,4,5,6,7]
i = 0

while i < len(llista) and llista[i] % 2 != 0:
    i+=1

print(f"El primer nombre parell ({llista[i]}) es troba a la posició {i}")
'''
# Comparació de llistes
'''
llista2 = [3,10000,100000,0] > [3,200,8000]
print(llista2)
'''
# Doble llista
'''
a = [[1,2,3],[4,5,6]]
print(a[1])
'''
# Llistes de una dimensió
'''
llista = range(1,5)
print(llista)
'''
# Llista de 3 dimensions ?¿!"?¿!
'''
b = [[[1,2,3],5,3],[4,5,6]]
print(b[0][1])
'''
# Introduir elements a una llista
'''
llista = []

i = 0
while i < 4:
    num = int(input("Introdueix 4 nombres: "))
    llista.append(num)
    i+=1

for i in range(4):
    num = int(input("Introdueix 4 nombres: "))
    llista.append(num)

print(llista)
'''
# Introduir elements a llista de dos elements o bidimensional



# Crea un programa q llegeix una matriu quadrada de dimensions (5x5)
# i indica si es tracta d'una matriu identitat o no.
'''
fil = int(input("Quantes files vols?: "))
col = int(input("Quantes columnes vols?: "))

llista = []

for i in range(fil):
    b = []
    for j in range(col):
        num = input("Introdueix nombres: ")
        b.append(num)
    llista.append(b)
# print(llista) # ???

for i in llista:
    a = ""
    for j in i:
        a+=f"{str(j).center(8):<12}"
    print("\n",a)
'''
# Dir si és una matriu identitat
'''
identitat = True

for i in range(len(llista)):
    for j in range(len(llista[0])):
        if i != j and llista[i][j] == 0 and identitat:
            identitat = True

        elif i == j and llista[i][j] == 1 and identitat:
            identitat = True
        
        else:
            identitat = False

print(identitat)
'''
## MATRIU_ conjunt de números
# 1 dimensional lists

# 1
# 13
# -2
# 0

# a = llista[i]  # Accedir elements

# llista[i] = a  # Modificar elements

# len(llista)  # Mida de llista

# 2 dimensional lists

#  13 | 0 | -1
#  2  | 2 | 8
#  0  | 0 | 301
#  4  |-12| 4

# a = llista[i][j]  # Accedir elements
     # [fila][colummna]

# llista[i][j]  # Modificar elements

# len(llista)  # Mida de la llista COLUMNES
# len(llista[0]) # Mida de la llista FILES, aquest cas la PRIMERA FILA