copa = int(input())

for i in range(copa):
    espais = " " * (41-i)
    fulles = "*"*(2*i+1)
    print(espais,fulles,sep="") # el SEP elimina l' espai de les
                                # comes del print

for i in range(4):
    espais = " "*(40)
    fulles = "*"*3
    print(espais,fulles,sep="")

