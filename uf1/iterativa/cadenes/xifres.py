#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Compta les xifres d'un nombre enter
#       - Comptant zeros a l'esquerra
#       - Passant primer el nombre a INT i després a STR
#
# Especificacions d'entrada: 
#   Nombre INT >= 0
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		  num
#   Execució 1    		  0003            1						  			 


# Comptant zeros a l'esquerra
# for (ESTA MALAMENT, NO COMPTA ELS 0 DESPRES D'UN NUM Q NO SIGUI 0)
'''
num = input("Introdueix un nombre: ")
cont = 0

for i in num:
    if ord(i) < 49 or ord(i) > 57:
        i = "0"
        cont += 1

    elif i == "0":
        cont += 1

xifres = len(num) - cont

print(xifres)
'''
# profe while
'''
num = input()

if num[0] == "+" or num[0] == "-":
        num = num[1:]

while num[0] == "0":
    num = num[1:]

print(len(num))
'''
# while



# Passant nombre a INT i després a STR
'''
num = input("Introdueix un nombre: ")

n_int = int(num)
n_str = str(n_int)

print(len(n_str))
'''

# llegim el número
nombre = input("Introdueix un nombre ")
suma = 0
comptador = 0

if nombre[0] == '+' or nombre[0] == '-':
	nombre = nombre[1:]

control = True
	
for caracter in nombre:
		if int(caracter) == 0 and control:
			pass
		
		else:	
			control = False
			suma += 1
			
print(suma)