#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Llegeix una paraula i dos nombres menors que la longitud
#   d'aquesta paraula, el primer menor que el segon.
#
#   Retorna la subcadena que comença en la posició del primer nombre
#   i acaba abans de la posició del segon.
#
# Especificacions d'entrada: 
#   Una cadena qualsevol i 2 nombre INT corresponents a la posició.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		
#   Execució 1

# Amb slicing
paraula = input()

a = int(input("Primer número: "))
b = int(input("Segon número: "))

solucio = "\n"

for i in range(a,b):
    solucio += paraula[i]
print(solucio)

# Sense slicing
