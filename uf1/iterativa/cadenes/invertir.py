#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Invertir una cadena de les maneres següents:
#       - Mostra caràcters un a un, ordre invers.       
#       - Crea una cadena buida on es va afegint cada caràcter
#         fins a tenir la cadena inversa.
#
# Especificacions d'entrada: 
#   Una cadena qualsevol.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		
#   Execució 1    									  			 
#	Execució 2	
# 	Execució 3	

# Mostrant caràcters un a un, en ordre invers.

cadena = input()
cont = len(cadena) - 1

for i in cadena:
    print(cadena[cont])
    cont -= 1

# Creant cadena buida on es van afegint els caràcters

