#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#     Rep una frase qualsevol i compta les vocals. Indica per
#     pantalla la vocal més abundant.  
#
# Especificacions d'entrada: 
#     Una cadena qualsevol.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		 frase
#   Execució 1    	 	 holaaa         La vocal més abundant és la a: 3		  			 

frase = input()

a = 0
e = 0
i = 0
o = 0
u = 0

for z in range(len(frase)):
    if frase[z] == 'a':
        a += 1
    if frase[z] == 'e':     
        e += 1
    if frase[z] == 'i': 
        i += 1
    if frase[z] == 'o':  
        o += 1
    if frase[z] == 'u': 
        u += 1

if a > e and a > i and a > o and a > u:
    print("La vocal més abundant és la a:",a)

if e > a and e > i and e > o and e > u:
    print("La vocal més abundant és la e:",e)

if i > a and i > e and i > o and i > u:
    print("La vocal més abundant és la i:",i)

if o > a and o > e and o > i and o > u:
    print("La vocal més abundant és la o:",o)

if u > a and u > e and u > i and u > o:
    print("La vocal més abundant és la u:",u)