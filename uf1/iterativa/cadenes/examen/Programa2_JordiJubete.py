#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Programa que crea una matriu i determina si un nombre concret
#   és un punt de sella.
#
#   És un punt de sella si el nombre a determinar és el mínim
#   de la seva fila i el màxim de la seva columna.
#
# Especificacions d'entrada: 
#    - 2 nombres INT per determinar les files i columnes.
#    - El nombre de nombres INT a introduïr a la matriu.


fil = int(input("Quantes files vols?: "))
col = int(input("Quantes columnes vols?: "))

matriu = []

for i in range(fil):
    b = []
    for j in range(col):
        num = input("Introdueix nombres: ")
        b.append(num)
    matriu.append(b)

for i in matriu:
    a = ""
    for j in i:
        a+=f"{str(j).center(8):<12}"
    print("\n",a,"\n")

print("ANÀLISI D'ELEMENT PER DETERMINAR SI ÉS UN PUNT DE SELLA:\n")

r = int(input("Introdueix la fila de l'element: "))
c = int(input("Introdueix la columna de l'element: "))


# No he sapigut fer-lo