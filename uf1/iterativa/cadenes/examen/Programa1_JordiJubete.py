#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#    Programa "sign up", demana a l'usuari nom o identificador 2 vegades
#    i valida si:
#          - els noms coincideixen.
#          - el nom és un usuari vàlid.
#          - l'usuari ja existeix en una llista d'inscrits. 
#         
#    Finalment, si és un usuari vàlid i no està inscrit en la llista d'usuaris,
#    l'introdueix a aquesta llista en primera posició i la mostra en ordre invers.
#
# Especificacions d'entrada: 
#  
#   String amb nom o identificador d'usuari.

llista_inscrits = ["PereP0uPr4t","Anna_83","Jordi","H4cK3r69","Aleix"]

nom = input("Introdueix nom d'usuari: ")
nom2 = input("Sisplau, torna'l a introduïr: ")

print("\n")

# Si els noms introduïts no coincideixen
if nom != nom2:
    print("Error: Els noms d'usuari introduïts no coincideixen.")

# Si el nom introduït NO és vàlid
elif not nom.isidentifier():
    print(f"Error: {nom} no és un nom d'usuari vàlid.")

# Si els noms introduits coincideixen
elif nom == nom2:
    valid = True

    for i in llista_inscrits:
        if llista_inscrits.count(nom):  # Si existeixen a la llista, error.
            valid = False
        
    if valid:
        print(f"Benvingut/da {nom}!\n")  
        llista_inscrits.insert(0,nom) # Si no existeixen a la llista, s'introdueix a la primera posició.
       
        for i in range(len(llista_inscrits)-1,-1,-1):
            if i != 0:
                print(llista_inscrits[i],end=", ")
            
        print(llista_inscrits[i])
        
    else:
        print(f"Error: L'usuari {nom} ja està inscrit.")