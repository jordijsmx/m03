#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Llegeix un nombre enter i determina si és capicua.
#
# Especificacions d'entrada: 
#   nombre INT qualsevol.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		  num
#   Execució 1           14341          És capicua.
#   Execució 2            5631          No és capicua.

num = input()

for i in range(len(num)):
    capicua = False

    if num[:1] == num[i:]:
        capicua = True
    
if capicua:
    print("És capicua.")

else:
    print("No és capicua.")