#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Llegeix un nombre enter positiu, seguidament, N nombres enters
#   positius i mostra el seu producte.
#       - No es poden utilitzar operadors, mètodes, funcions,
#         potències ni arrels.
#
# Especificacions d'entrada: 
#  
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		
#   Execució 1 