num1 = int(input())
num2 = int(input())

trobat = False
n = 1

while not trobat:
    if n % num1 == 0 and n % num2 == 0 and (num1 * num2 == n):
        print("El mínim comú múltiple és:", n)
        trobat = True
    n += 1