#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Llegeix dos nombres enters positius i mostra per pantalla
#   el seu mínim comú múltiple.
#
# Especificacions d'entrada: 
#   2 nombres INT positius > 0.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 	num1    num2            
#   Execució 1       5        8          El seu mínim comú múltiple és 40.
#   Execució 2       8        2          El mínim comú múltiple és 8.



        