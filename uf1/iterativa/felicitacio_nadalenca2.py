# Felicitació nadalenca 
# Fes un programa que passi un nombre enter positiu per argument i 
# dibuixi un avet de nadal.
'''
possibles mètodes: center

característiques:
- la copa de l'arbre ha de tenir tantes fileres com el nombre que es llegeix
- utilitzarem el caràcter '*' per dibuixar-lo
- la primera filera de la copa tindrà un únic '*'
- el tronc sempre serà un rectangle de 3 x 4
- l'avet s'ha de dibuixar centrar a la meitat de la pantalla
'''

nombre = int(input("Introdueix un nombre "))
comptador2 = 1
comptador3 = 1

while comptador2 <= nombre:
	if comptador3 % 2 != 0:
		tronc = comptador3 * '*'
		tronc = tronc.center(50)
		print(tronc)
		comptador2 += 1
	comptador3 += 1

	
ALTURA = 3 * '*'
AMPLITUD = 4 

for i in range(1,AMPLITUD+1):
	comptador = 1
	tronc = comptador * ALTURA
	tronc = tronc.center(50)
	print(tronc)