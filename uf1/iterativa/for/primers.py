#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# 
#
# Especificacions d'entrada: 
# 
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		
#   Execució 1    									  			 
#	Execució 2	
# 	Execució 3

# Mostra si un nombre es primer o no
'''
num = int(input())
primer = True

for i in range(2,num):
    if num % i == 0:
        primer = False

print(primer)
'''
# Mostra els primers menors que n
'''
n = int(input())
num = 2

for i in range(1,n):
    
    primer = True
    
    for j in range(2,num):
        if num % j == 0:
            primer = False

    if primer:    
        print(num)
    num += 1

n = int(input())
num = 2

for i in range(1,n):
    primer = True
    div = 2
    while div < num:
        if num % div == 0:
            primer = False
        div += 1
    if primer:    
        print(num)
    num += 1
'''
# Mostra els n primers primers

n = int(input())
num = 2
i = 0

while i < n:
    
    primer = True
    for j in range(2,num):
        if num % j == 0:
            primer = False
    
    if primer:
        print(num)
        i += 1

    num += 1