#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Programa que llegeix un nombre enter i ens mostri 
#   les xifres que té.
#
# Especificacions d'entrada: 
#   Nombre INT >= 0
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		   num
#   Execució 1    		   25			   2				  			 
#	Execució 2	           777             3
# 	Execució 3	          46478            5
'''
num = input() # Strings es poden recórrer, números no.

xifres = 0

for i in num:
    xifres += 1

print("Aquest nombre té",xifres,"xifres.") 
'''
# amb while
'''
num = int(input())

xifres = 0

while num > 0:
    num //= 10
    xifres += 1
if xifres == 0:
    xifres += 1

print(xifres)
'''
# versió 2
'''
num = int(input())

xifres = 0

mult = 1

while mult <= num:
    mult *= 10
    xifres +=1
if xifres == 0:
    xifres += 1
print(xifres)
'''

num = int(input())

xifres = 1

for i in range(1,num+1):
    print(num)

print("Aquest nombre té",xifres,"xifres.") 


# Suma de les xifres d'un nombre
'''
num = input()

sum = 0

for i in num:
    sum += int(i)
   
print(sum)
'''
# Amb while
'''
num = int(input())

sum = 0

while num != 0:
    sum += num % 10
    num //= 10
    
print(sum)
'''