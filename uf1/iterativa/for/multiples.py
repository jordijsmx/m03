#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Llegeix dos nombres enters positius i calcula els n primers 
#   múltiples de m
#
# Especificacions d'entrada: 
#   2 nombres INT >= 0
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 	   n       m
#   Execució 1    	  10       2         2,4,8...  							  			 
#	Execució 2	
# 	Execució 3

n = int(input())
m = int(input())

for i in range(n):
    print(m)
    m += m