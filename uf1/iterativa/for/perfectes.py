#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   1 - Programa que llegeix un nombre i diu si és perfecte o no.
#   2 - Llegeix un nombre enter n >= 0 i mostra els n primers nombres.
# Especificacions d'entrada: 
#   Nombre INT >= 0
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		
#   Execució 1    									  			 
#	Execució 2	
# 	Execució 3	  

# for (Programa 1)
'''
num = int(input())

cont = 0

for i in range(1,num):
    if num % i == 0:
        cont += i  

if cont == num:
    print('És perfecte.')
else:
    print("No és perfecte.")
'''
# while (Programa 1)
'''
num = int(input())
i = 1
cont = 0

while num > cont:
    if num % i == 0:
        cont += i
    i += 1

if cont == num:
    print('És perfecte.')
else:
    print("No és perfecte.")
'''
# LA LOCURA DE ALEIX (Programa 2)
'''
vegades = int(input())
num= 0
suma = 0
numero = 4
perfectes = 0
num_divisors = 0

while vegades != perfectes :
    num +=1
    if numero % num == 0 :
        suma += num    
    if numero == suma  :
        if numero / 2 == num:
            print(numero)
            perfectes += 1
            numero += 1
            num = 0
            suma = 0
    elif numero < suma:
        numero += 1
        num = 0
        suma = 0
'''

# Programa 2 amb WHILE

trobats = int(input("Introdueix quants nombres perfectes vols: "))

a = 0
n = 2


while a < trobats:

    sum = 0

    for i in range(1,n):
        if n % i == 0:
            sum += i

    if sum == n:
        print(n)
        a += 1

    n += 1