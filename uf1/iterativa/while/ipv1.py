#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# Escriu una expressió booleana que digui si 4 camps llegits
# corresponen a una adreça IP vàlida o no.
#
# Especificacions d'entrada: 
# 4 nombres INT qualsevols.
# 
#
# Joc de proves:
#						Entrada 		Sortida

#                 ip1 ip2 ip3 ip4        valida	
#   Execució 1    192 168  1   0         True							  			 
#	Execució 2	   2   -3 34  729        False

ip1 = int(input())
ip2 = int(input())
ip3 = int(input())
ip4 = int(input())

valida = ((ip1 >= 0 and ip1 <= 255) and
         (ip2 >= 0 and ip2 <= 255) and
         (ip3 >= 0 and ip3 <= 255) and
         (ip4 >= 0 and ip4 <= 255))

print(valida)