#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete

# Escriu els múltiples de dos menors o iguals a 100

i = 2

while i <= 100:
    print(i)
    i += 2

'''
numero = 1

multiple = int(input("Introdueix un nombre sobre el qual calcular els seus múltiples: "))

limit = int(input("Introduexi un nombre natural límit: "))

while numero <= limit:
    if numero % multiple == 0:
        print(numero)
    numero += 1
'''
# Escriu els 10 primers múltiples de dos:
'''
mult = 2
i = 1

while i <= 10:
    print(mult)
    i += 1
    mult += 2
'''
'''
mult = int(input())
lim = int(input())

num = 1
cont = 1

while cont <= lim:
    if num % mult == 0:
        print(num)
        cont +=1
    num+=1
'''  
# Escriu els n primers múltiples de m. 
'''
num = int(input()) 
mult = int(input()) 

i = 1
num1 = mult

while i <= num:
    print(num1)
    num1 += multiple
    i += 1
'''

'''
div = int(input())
lim = int(input())

num = 1

while num < lim:
    if num % div == 0
    print(num)

num += 1
'''

# Sumar els múltiples de 2 menors de 100.
'''
cont = 2
suma = 0

while cont < 100:
    suma += cont
    cont += 2

print(suma)
'''
'''
div = int(input())
lim = int(input())

num = 1
suma = 0

while num < lim:
    if num % div == 0:
        suma += num
    num += 1
print(suma)
'''

# Sumar les potències de n menors de 100.
'''
n = int(input())

i = 0
suma = 0

while n**i < 100:
    pot = n ** i
    suma += pot
    i += 1

print(suma)
'''
'''
base = int(input())
lim = int(input())

n = 1
suma = 0
potencia = base

while potencia < lim:
    suma += potencia
    n += 1
    potencia = base**n
    
print(suma)
'''