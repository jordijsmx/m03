#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# Llegeix una quantitat prefixada de nombres i es calcula
# la mitjana dels positius.
#
# Especificacions d'entrada: 
# Qualsevol número de nombres positius i un de negatiu
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		
#   Execució 1    	   5,3,4,-5,8          5								  			 


NUM = 5

i = 0
suma = 0
compt = 0

while compt < NUM:
    num = float(input())

    if num < 0:
        num = 0
        i -= 1
    
    suma += num
    i += 1
    compt += 1
    
mitjana = suma / i
print(mitjana)

'''
vegades = int(input('Numero de vegades que vols posar numeros: '))
cont = 0
sum_num = 0
div = 0
while vegades > cont :
    num = float(input('Introdueix un numero: '))
    if num < 0 :
        num = 0
        div -=1
    sum_num += num
    div +=1
    cont += 1

mitjanav3= sum_num /div
print(mitjanav3)
'''