#!/usr/bin/python3
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Llegeix un nombre INT i mostra els seus divisors en ordre
#   ascendent.
#
# Especificacions d'entrada: 
#  Un nombre INT > 0
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 	      num1	
#   Execució 1    	 	   12          1,2,3,4,6,12

num = int(input("Introdueix un nombre: "))			 

i = 1

while i <= num:
    if num % i == 0:
        print(i)
    i += 1