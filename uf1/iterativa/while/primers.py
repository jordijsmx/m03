#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Llegeix un nombre i diu si és primer o no.
#
#   Un nombre primer és aquell que no té més divisors que el 1
#   i ell mateix.
#
# Especificacions d'entrada: 
#   Un nombre qualsevol.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		  num
#   Execució 1    		   2             True						  			 
#	Execució 2	           6             False

# Mostra si un nombre es primer o no
'''
num = int(input())
i = 2

primer = True

while num > i and primer:
    if num % i == 0:
        primer = False
    i += 1
print(primer)
'''
# Mostrar els nombres primers menors que n

n = int(input())

num = 2

while n > num:
    i = 2
    primer = True
    while num > i:
        if num % i == 0:
            primer = False
        i += 1

    if primer:
        print(num)
    num += 1

# Mostrar els n primers primers
'''
num = int(input())
num2 = 2
i = 0

while i < num:
    div = 2
    primer = True
    while div < num2:
        if num2 % div == 0:
            primer = False
        div += 1

    if primer:
        print(num2)
        i += 1
    
    num2 += 1
'''