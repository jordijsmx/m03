#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Introdueix un nombre entre 1 i 100 i el programa et diu si has 
#   endevinat el nombre o no.
#
# Especificacions d'entrada: 
#   Un nombre INT > 0
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		
#   Execució 1    									  			 
#	Execució 2	
# 	Execució 3

import random

num = 0
aleatori = random.randint(1,100)

while aleatori != num:
    num = int(input("Introdueix un número entre 1 i 100: "))

    if not (num >= 1 and num <= 100):
        print("El número ha de ser entre 1 i 100")

    elif num < aleatori:
        print("El número és més gran.")

    elif num > aleatori:
        print("El número és més petit.")

    elif num == aleatori:
        print("Has encertat, el número era",aleatori)