#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Llegeix nombres positius fins que s'introdueix un nombre negatius
#   En aquell moment es calcula la mitjana dels nombres entrats.
#
# Especificacions d'entrada: 
#   Qualsevol número de nombres positius i un de negatiu
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		
#   Execució 1    	    5,3,4,-5           4								  			 


i = -1
suma = 0
num = 0

while num >= 0:
    suma += num
    i += 1
    num = float(input("Introdueix un nombre: "))

print(suma/i)