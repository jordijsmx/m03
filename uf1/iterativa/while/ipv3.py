#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Escriu una expressió booleana que digui si 4 camps llegits
#   corresponen a una adreça IP vàlida o no.
#       SURT DEL PROGRAMA QUAN TROBA UN VALOR INTRODUIT FALSE.
#
# Especificacions d'entrada: 
#   4 nombres INT qualsevols.
#
# Joc de proves:
#						Entrada 		Sortida
#
#                 ip1 ip2 ip3 ip4        valida	
#   Execució 1    192 168  1   0         True							  			 
#	Execució 2	   2   -3 34  729        False

i = 0
valida = True

while i < 4 and valida:
    ip = int(input())

    if not (ip >= 0 and ip <= 255):
        valida = False
        print("IP no vàlida.")

    i += 1

if valida:
    print("IP vàlida.")