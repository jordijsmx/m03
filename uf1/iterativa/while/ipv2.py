#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#   Escriu una expressió booleana que digui si 4 camps llegits
#   corresponen a una adreça IP vàlida o no.
#
# Especificacions d'entrada: 
#   4 nombres INT qualsevols.
#
# Joc de proves:
#						Entrada 		Sortida
#
#                 ip1 ip2 ip3 ip4        valida	
#   Execució 1    192 168  1   0         True							  			 
#	Execució 2	   2   -3 34  729        False

i = 0
valida = True

while i < 4:
    ip = int(input())
    if not (ip >= 0 and ip <= 255):
        valida = False
    
    i += 1

print(valida)

'''
cont = 0
ip_valida = True

while cont < 4:
    camp = int(input())
    if not (camp >= 0 and camp <= 255):
        ip_valida = False
    
    cont += 1

if ip_valida:
    print("IP vàlida.")

else:
    print("IP no vàlida.")
'''