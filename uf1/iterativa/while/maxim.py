#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# Programa que llegeix 3 nombres i mostra el màxim d'aquests.  
#
# Especificacions d'entrada: 
# 3 nombres INT >= 0 qualsevols.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 num1   num2   num3      max		
#   Execució 1     8      0       3        8										  			 
#	Execució 2	   7      13	 13 	   13
# 	Execució 2	   20     19	 18 	   20

max = 0

i = 0

while i < 3:
    num = int(input())

    if num > max:
        max = num
    i += 1
    
print(max)