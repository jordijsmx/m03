#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# Programa que fa de compte enrere a partir d'un nombre
# introduït. Quan arriba a 0 explota.
#
# Especificacions d'entrada: 
# Nombre INT > 0.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		 num              num  
#   Execució 1    		  5			 5,4,3,2,1, Boom.			  			   

num = int(input("Introdueix un número per començar el compte enrere: "))

while num > 0:
    print(num)
    num -= 1

print('Boom.')