#!/usr/bin/python3
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# Programa que llegeix 3 nombres i mostra el màxim d'aquests.  
#
# Especificacions d'entrada: 
# 3 nombres INT >= 0 qualsevols.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 num1   num2   num3      max		
#   Execució 1    										  			 
#	Execució 2	  
# 	Execució 2	 
'''
max = 0

i = 0

rep = int(input("Introdueix el número màxim de repeticions: "))

if rep > 0:

    while i < rep:
        num = int(input("Introdueix un número: "))

        if num > max:
            max = num
        i += 1

    print("El número màxim és: ",max)

else:
    print("Has introduït ",rep)
'''
# Per a que accepti números negatius:

rep = int(input("Introdueix el número màxim de repeticions: "))

if rep > 0:
    num1 = int(input("Introdueix un número: "))
    max = num1
    i = 0
    while i < rep -1:
        num = int(input("Introdueix un número: "))
        if num > max:
                max = num
        i += 1

    print("El número màxim és: ",max)