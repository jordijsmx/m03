#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#       Multiplica dos nombres llegits, pero sumant-lo amb
#       amb si mateix tantes vegades com indica l'altre
#
# Especificacions d'entrada: 
#       2 nombres INT positius
# 
# Joc de proves:
#						Entrada 		Sortida
#                    num1   num2        
#   Execució 1    	  4		  5	           20

# Multiplicació en base a sumes successives

num1 = int(input("Escriu un nombre: "))
num2 = int(input("Escriu les vegades que el vols sumar: "))
sum = 0
i = 0

while i < num2:
    sum += num1
    i += 1
print(sum)