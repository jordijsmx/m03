#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# 
# Fes un programa que llegeixi un nombre 
# i en mostri el valor absolut.
# 
# Especificacions d'entrada: 
# 
#  Un nombre qualsevol.
#
# Joc de proves:
#						Entrada 		Sortida
#		Execució 1         1              1			
#						  			 
#		Execució 2		   0              0		
# 
# 	    Execució 3		  -1              1

num = float(input())

if num >= 0:
    print(num)
else:
    num = num * -1
    print(num)

