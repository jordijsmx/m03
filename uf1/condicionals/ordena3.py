
#capçalera complerta.

#Atenció: En l'exercici del mobil i el bocata, el que has après a fer és un
#intercanvi de variables. Les cadenes 'bocata' o 'mobil', es poden substituïr
#per qualsevol nombre i estaràs intercanviant el valor de dues variables
#utilitzant una variable auxiliar

#1. llegim 3 nombres int

num1 = int(input())
num2 = int(input())
num3 = int(input())

#2. si el primer és major que el segon, intercanviem els valors de les 
#variables (canviem de ma el mobil i el bocata)

if num1 > num2:   
    var_aux = num2 
    num2 = num1     
    num1 = var_aux

#3. si el segon és major que el tercer, intercanviem els valors de les 
#variables  (canviem de ma el mobil i el bocata)

if num1 > num3:
    var_aux = num3
    num3 = num1
    num1 = var_aux

if num2 > num3:
    var_aux = num3
    num3 = num2
    num2 = var_aux

#4. mostrem el resultat ordenat de menor a major

print(num1,num2,num3)