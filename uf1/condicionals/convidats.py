#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#
# Especificacions d'entrada: 
#
# Qualsevol nombre INT >= 2 i <= 431
#
# Joc de proves:
#						Entrada 		Sortida
#		Execució 1         38		       4  taules     2 sobrants
#						  			    
#		Execució 2	       431             36  taules    11 sobrants

TAULES = 12

convidats = int(input())

taules = convidats // TAULES

persones_sobrants = convidats % TAULES

if persones_sobrants != 0:
    taules += 1
    print(taules,"taules totals i sobren",persones_sobrants,"persones.")

else:
    print(taules,"taules totals i sobren",persones_sobrants,"persones.")