#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#
#  Fes un programa que llegeixi un nombre i digui 
#  quin dia de la setmana és. Per
#  exemple, el 0 és dilluns, el 1 és dimarts, etc...
# 
# Especificacions d'entrada: 
#
#  Un nombre INT >= 0 i <=6
#
# Joc de proves:
#						Entrada 		Sortida
#		Execució 1        0              Dilluns 				
#						  			 
#		Execució 2		  1              Dimarts			 

dia = int(input())

if dia == 0:
    print("Dilluns")
elif dia == 1:
    print("Dimarts")
elif dia == 2:
    print("Dimecres")
elif dia == 3:
    print("Dijous")
elif dia == 4:
    print("Divendres")
elif dia == 5:
    print("Dissabte")
elif dia == 6:
    print("Diumenge")
else:
    print("Introdueix un nombre de 0 a 6")
