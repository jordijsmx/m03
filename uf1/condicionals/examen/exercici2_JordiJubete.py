#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#  Programa que llegeix un nombre de entre 1 i 5 xifres. Mostra un missatge amb el
#  número de xifres que té
#
# Especificacions d'entrada: 
#  Nombre INT positiu >= 0 d'entre 1 i 5 xifres.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 		  num
#   Execució 1    		  567           El nombre té 3 xifres							  			 
#	Execució 2	         23418          El nombre té 5 xifres
# 	Execució 3	           2            El nombre té 1 xifres

num = str(input())

if num == num[0]:
    print("1 xifra")

elif num == num[0:2]:
    print("2 xifres")

elif num == num[0:3]:
    print("3 xifres")

elif num == num[0:4]:
    print("4 xifres")

elif num == num[0:5]:
    print("5 xifres")

else:
    print("Introdueix un nombre de 1 a 5 xifres.")