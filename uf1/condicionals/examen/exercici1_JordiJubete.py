#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#  Programa en el qual s'entren 3 nombres de 3 xifres i un quart d'una sola
#  xifra. Mostra com a sortida els nombres que tinguin més xifres iguals
#  a la quarta.
#
# Especificacions d'entrada: 
#  3 nombres de tupus cadena >= 0 de 3 xifres cadascún i un quart nombre INT >= 0 d'una sola
#  xifra.
# 
# Joc de proves:
#						Entrada 		            Sortida
#                num1     num2    num3     ref         	
#   Execució 1   232      567      112      2       232, 112									  			 
#	Execució 2	 067      879      434      9         879 
# 	Execució 3   777      665      856      0       Cap xifra coincideix amb el de referència.
# 	Execució 4	 768      072      097      7       768, 072, 097

num1 = input()
num2 = input()
num3 = input()
ref = input()

if num1[0] == ref or num1[1] == ref or num1[2] == ref:
    print(num1)

if num2[0] == ref or num2[1] == ref or num2[2] == ref:
    print(num2)

if num3[0] == ref or num3[1] == ref or num3[2] == ref:
    print(num3)

else:
    print("Cap xifra introduïda coincideix amb la de referència.")