#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#  Programa que llegeix quatre noms i els mostra en al sortida ordenats
#  per ordre alfabètic ascendent.
#
# Especificacions d'entrada: 
#  4 entrades de tipus cadena.
# 
# Joc de proves:
#						   Entrada 		                  Sortida
#                 nom1   nom2     nom3      nom4	
#   Execució 1   zaira   jordi	 cristina	marc		  cristina jordi marc zaira			  			 
#	Execució 2	 anna    ferran  Jordi      Gabriel       Gabriel Jordi anna ferran

nom1 = str(input())
nom2 = str(input())
nom3 = str(input())
nom4 = str(input())

if nom1 > nom2:
    var_aux = nom2
    nom2 = nom1
    nom1 = var_aux

if nom1 > nom3:
    var_aux = nom3
    nom3 = nom1
    nom1 = var_aux

if nom1 > nom4:
    var_aux = nom4
    nom4 = nom1
    nom1 = var_aux

if nom2 > nom3:
    var_aux = nom3
    nom3 = nom2
    nom2 = var_aux

if nom2 > nom4:
    var_aux = nom4
    nom4 = nom2
    nom2 = var_aux

if nom3 > nom4:
    var_aux = nom4
    nom4 = nom3
    nom3 = var_aux

print(nom1,nom2, nom3, nom4)