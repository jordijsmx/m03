#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
#  Programa que llegeix el valor de les notes de 2 exàmens. Mostra per pantalla
#  la nota mitjana. 
#           - Si no s'ha realitzat, s'introdueix "NP".
#           - Si un dels dos exàmens no s'ha realitzat, es mostra la nota del que s'ha realitzat.
#           - Si cap exàmen s'ha realitzat, es mostra "No s'ha realitzat cap dels dos exàmens." 
#
# Especificacions d'entrada: 
#   2 nombres de tipus CADENA.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 	nota1     nota2    	
#   Execució 1    	 NP         NP       No s'ha realitzat cap dels dos exàmens.				  			 
#	Execució 2	     7          NP       7
# 	Execució 3	     NP         4.30     4.30
#   Execució 3	     6          8        7.0
#   Execució 4       2.50       6.35     4.425
#   Execució 5       7          3.32     5.16

# Entrem els dos valors
nota1 = str(input())
nota2 = str(input())

# Primer fem la comparacio de si els dos valors són "NP" i, si és així, mostrem el resultat:
if nota1 == "NP" and nota2 == "NP":
    print("No s'ha realitzat cap dels dos exàmens.")

# Fem la comparació de si només un dels valors és "NP", si és aixi, mostrem el valor que no ho és:
elif nota1 == "NP":
    print(nota2)

elif nota2 == "NP":
    print(nota1)

# Per últim, si tots dos valors no són "NP" els transformem a tipus FLOAT, calculem la mitjana i 
# mostrem el resultat:
else:
    mitjana = (float(nota1) + float(nota2)) / 2
    print(mitjana)