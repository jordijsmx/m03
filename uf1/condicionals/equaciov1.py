#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# 
#
#
#
# Especificacions d'entrada: 
# 
#  3 nombres float a, b i c, corresponents als coeficients de
#  l’equació ax² + bx + c = 0. L’única restricció és que a ha de ser 
#  diferent de zero.
#
# Joc de proves:
#						Entrada 		Sortida
#		Execució 1      			
#						  			 
#		Execució 2		


a = float(input())
b = float(input())
c = float(input())

if a != 0:

    diferencial = (b**2 - 4*a*c)

    x1 = (-b + (diferencial**(1/2))) / (2*a)
    x2 = (-b - (diferencial**(1/2))) / (2*a)

    if diferencial < 0:
        print("No té solució.")

    elif diferencial > 0:
        print(x1)
        print(x2)
    else:
        print(x1)