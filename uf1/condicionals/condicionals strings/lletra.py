#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# Llegeix una lletra I escriu per pantalla un missatge
# indicant si la lletra és majúscula o minúscula.
#
# Especificacions d'entrada: 
# Un caràcter string qualsevol.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#		                lletra
#   Execució 1             a          "És minúscula."
#   Execució 2             Z          "És majúscula."

lletra = str(input())

if lletra >= 'a' and lletra <= 'z':
    print(lletra,"és minúscula.")
elif lletra >= 'A' and lletra <= 'Z':
    print(lletra,"és majúscula.")
else:
    print("Introdueix una lletra.")