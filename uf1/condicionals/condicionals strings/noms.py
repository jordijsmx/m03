#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# Llegeix dos noms I els imprimeix per pantalla
# ordenats en ordre alfabètic ascendent.
#
# Especificacions d'entrada: 
# Dos strings qualsevols
# 
#
# Joc de proves:
#						Entrada 		Sortida
#		            nom1    nom2
#   Execució 1     sofia    albert      albert sofia 				
   			 

nom1 = str(input())
nom2 = str(input())

if nom1 > nom2:
    var_aux = nom2
    nom2 = nom1
    nom1 = var_aux

print(nom1,nom2)