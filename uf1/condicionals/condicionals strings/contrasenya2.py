#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# Demana a l’usuari la introducció d’una contrasenya i
# la llegeix, I demana a l’usuari que la repeteixi, llegint també la repetició de la
# contrasenya. El programa escriu per pantalla “Contrasenya vàlida” si les dues
# contrasenyes coincideixen I “Contrasenya invàlida” si les dues contrasenyes no
# coincideixen.
#
# Especificacions d'entrada: 
# 
# 
#
# Joc de proves:
#						Entrada 		Sortida
#		                
#   Execució 1                      
#   Execució 2                      

passwd = str(input("Introdueix una contrasenya: "))
passwd2 = str(input("Repeteix la contrasenya: "))

'''
if passwd != passwd2:
    print("Les dues contrasenyes no corresponen.")

elif not len(passwd) > 8:
    print("La contrasenya és massa curta.")

elif not (passwd[0] >= 'A' and passwd[0] <= 'Z'):
    print("La primera lletra no és una lletra llatina majúscula.")

else:
    print("Contrasenya vàlida.")

'''
if passwd == passwd2:
    if len(passwd) > 8:
        if passwd[0] >= 'A' and passwd[0] <= 'Z':
            print("Contrasenya vàlida.")
        else:
            print("La primera lletra no és majúscula.")
    else:
        print("La contrasenya és massa curta.")
else:
    print("Les dues contrasenyes no corresponen.")