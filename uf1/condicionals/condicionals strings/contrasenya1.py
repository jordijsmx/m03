#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# Demana a l’usuari la introducció d’una contrasenya i
# la llegeix, I demana a l’usuari que la repeteixi, llegint també la repetició de la
# contrasenya. El programa escriu per pantalla “Contrasenya vàlida” si les dues
# contrasenyes coincideixen I “Contrasenya invàlida” si les dues contrasenyes no
# coincideixen.
#
# Especificacions d'entrada: 
# 
# 
#
# Joc de proves:
#						Entrada 	    	Sortida
#		           passwd  passwd2  
#   Execució 1      pass     pass      "Contrasenya vàlida.""
#   Execució 2      pass     p4ss      "Contrasenya invàlida."

passwd = str(input("Introdueix una contrasenya: "))
passwd2 = str(input("Repeteix la contrasenya: "))

if passwd == passwd2:
    print("Contrasenya vàlida.")
else:
    print("Contrasenya invàlida.")