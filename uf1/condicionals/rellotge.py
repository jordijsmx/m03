#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# 
#   Fes un programa que simuli un rellotge. Les dades d’entrada han de ser les hores,
#   minuts i segons i la sortida el segon següent tal i com es mostraria en el 
#   rellotge.
#
#
# Especificacions d'entrada: 
#  3 nombres INT, les hores >=0 a 23, minuts >= 0 a 59, segons >= 0 a 59.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#		Execució 1      23 59 59         0 0 0				
#						  			 
#		Execució 2		22 58 59    	22 59 00		 

hores = int(input())
minuts = int(input())
segons = int(input())

#segons += 1
if segons == 60:
    segons = 0
    minuts += 1
    if minuts == 60:
        minuts = 0
        hores += 1
        if hores > 23:
            hores = 0

print(hores,minuts,segons)
  

segon_seg = segons + 1

if segon_seg == 60 :
    segon_final = 0
    minut_seg = minuts + 1
    if minut_seg == 60 :
        minut_final = 0
        hora_seg = hores + 1
        if hora_seg == 24 :
            hora_final = 0
            print(hora_final,':',minut_final,':',segon_final)
        elif hora_seg < 24 :
            print(hora_seg,':',minut_final,':',segon_final)

    elif minut_seg < 60 :
        print(hores,':',minut_seg,':',segon_final)

elif segon_seg < 60:
    print(hores,':',minuts,':',segon_seg)