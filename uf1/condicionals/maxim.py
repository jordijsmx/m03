#!/usr/bin/python3 
#-*- coding: utf-8-*- 
# 
# Escola del Treball de Barcelona 
# Administració de Sistemes informàtics
# Curs 2022-2023
# 
# Autor: Jordi Jubete
#
# Descripció: 
# Programa que llegeix 3 nombres i mostra el màxim d'aquests.  
#
# Especificacions d'entrada: 
# 3 nombres INT >= 0 qualsevols.
# 
#
# Joc de proves:
#						Entrada 		Sortida
#                 num1   num2   num3      max		
#   Execució 1     8      0       3        8										  			 
#	Execució 2	   7      13	 13 	   13
# 	Execució 2	   20     19	 18 	   20

num1 = int(input())


'''
if num1 >= num2 and num1 >= num3:
    max = num1

elif num2 >= num1 and num2 >= num3:
        max = num2

else:
    max = num3

print(max)
'''

max = 0

if num1 > max:
    max = num1

num2 = int(input())

if num2 > max:
    max = num2

num3 = int(input())

if num3 > max:
    max = num3

print(max)