
# capçalera complerta

'''
joc de proves
Entrada 			sortida
10, 5				True
5, 10				True
3,  2				False
-10, -2				True
-3, 2				False
-10, 5				True

Especificacions d'Entrada: 2 nombres enters diferents de 0
'''

#ATENCIÓ: Aquest programa ha de seguir aquest refinament. En cas contrari,
#serà un zero ni que funcioni correctament

#llegim dos enters
n1 = int(input())
n2 = int(input())

#els modifiquem (si cal) per tenir els valors absoluts del dos nombres 

if n1 < 0:
    n1 = -n1
    
if n2 < 0:
    n2 = -n2

'''
n1 = (n1**2)**(1/2)
n2 = (n2**2)**(1/2)
'''

#intercanviem les variables (si cal) perquè el primer sigui major que el 
#segon

if n1 < n2:
    var_aux = n2
    n2 = n1
    n1 = var_aux

#mostrem si el segon és divisor del primer o no
print(n1 % n2 == 0)