ip1 = int(input())
ip2 = int(input())
ip3 = int(input())
ip4 = int(input())

validacio = (ip1 == 10 and(ip2 >= 0 and ip2 <= 255) and 
            (ip3 >= 0 and ip3 <= 255) and 
            (ip4 >= 0 and ip4 <= 255) or
            (ip1 == 172 and ip2 == 16) and 
            (ip3 >= 0 and ip3 <= 255) and 
            (ip4 >= 0 and ip4 <= 255) or
            (ip1 == 192 and ip2 == 168) and 
            (ip3 >= 0 and ip3 <= 255) and
            (ip4 >= 0 and ip4 <= 255))

validacio2 = ((ip1 == 10 or (ip1 == 172 and ip2 == 16) or 
              ip1 == 192 and ip2 == 168) or 
              (ip4 == 0 or ip4 == 255) 

              and ip1 >= 0 and ip1 <= 255 and ip2 >= 0
              and ip2 <= 255 and ip3 >= 0 and ip3 <= 255
              and ip4 >= 0 and ip4 <= 255)

print(validacio)
print(validacio2)


#metode 1 


validar_ip1 = ((ip1 == 10 and (ip2 >= 0 and ip2 <= 255) and (ip3 >= 0 and ip3 <= 255) and 
              (ip4 >= 0 and ip4 <= 255)) or 
              (ip1 == 172 and ip2 == 16 and 
              (ip3 >= 0 and ip3 <= 255) and 
              (ip4 >= 0 and ip4 <= 255)) or 
              (ip1 == 192 and ip2 == 168 and 
              (ip3 >= 0 and ip3 <= 255) and 
              (ip4 >= 0 and ip4 <= 255)))

print(validar_ip1)


#metode 2 (millor)

validar_ip = ip1 == 10 or (ip1 == 172 and ip2 == 16) or (ip1 == 192 and ip2 == 168) or (ip4 == 0 or ip4 == 255) and (ip1 >= 0 and ip1 <= 255) and (ip2 >= 0 and ip2 <= 255) and (ip3 >= 0 and ip3 <= 255) and (ip4 >= 0 and ip4 <= 255)

print(validar_ip)