def get_alumne_n(llista, n):
  print(f"{llista[n][1]} {llista[n][2]}, dni: {llista[n][0]}")

def get_alumnes(llista):
  for alumne in llista: 
    print(f"{alumne[1]} {alumne[2]}, dni: {alumne[0]}")

def get_alumnes_nom(llista):
  for i in range(1,len(llista)):
    key = llista[i]
    continuar = True
    a = i-1
    while a >= 0 and continuar:
      if key[1] < llista[a][1]:
        llista[a+1] = llista[a]
        llista[a] = key
        a -= 1
      else: 
        continuar = False

  get_alumnes(llista)

def get_alumnes_cognom(llista):
  for i in range(1,len(llista)):
    key = llista[i]
    continuar = True
    a = i-1
    while a >= 0 and continuar:
      if key[2] < llista[a][2]:
        llista[a+1] = llista[a]
        llista[a] = key
        a -= 1
      else: 
        continuar = False

  get_alumnes(llista)

def get_alumnes_dni(llista):
  for i in range(1,len(llista)):
    key = llista[i]
    continuar = True
    a = i-1
    while a >= 0 and continuar:
      if key[0] < llista[a][0]:
        llista[a+1] = llista[a]
        llista[a] = key
        a -= 1
      else: 
        continuar = False

  get_alumnes(llista)

if __name__ == "__main__":

  dadesAlumnes = [['12345678A', 'jordi', 'perez'], ['52345678A',
'manel', 'garcia'], ['62345678A', 'joan-ramon', 'vila'],
['72345678A', 'maria', 'casas'], ['11111111D', 'josep-lluís', 'márquez']]

  get_alumne_n(dadesAlumnes, 3)
  print("\n\n")
  get_alumnes(dadesAlumnes)
  print("\n\n")
  get_alumnes_dni(dadesAlumnes)
  print("\n\n")
  get_alumnes_nom(dadesAlumnes)
  print("\n\n")
  get_alumnes_cognom(dadesAlumnes)
  print("\n\n")
