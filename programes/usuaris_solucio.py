import sys

def file2dic():

  dicc = {}
  with open("Usuaris.txt", "r") as f:
    id = f.readline().strip().split("|")[1:]
    nom_usuari = f.readline().strip().split("|")[1:]
    contrasenya = f.readline().strip().split("|")[1:]
    edat = f.readline().strip().split("|")[1:]
    privilg = f.readline().strip().split("|")[1:]

    for i in range(len(id)):
      dicc[f"{id[i]:<20}"] = [f"{nom_usuari[i]:<20}", f"{contrasenya[i]:<20}", f"{edat[i]:<20}", f"{privilg[i]:<20}"]

    return dicc


def dic2file(dcc):

  id_prl = "Id"
  nom_usuari_prl = "Nom_Usuari"
  contrasenya_prl = "Contrasenya"
  edat_prl = "Edat"
  privilg_prl = "Privilegi"


  id = [f"{id_prl:<20}"]
  nom_usuari = [f"{nom_usuari_prl:<20}"]
  contrasenya = [f"{contrasenya_prl:<20}"]
  edat = [f"{edat_prl:<20}"]
  privilg = [f"{privilg_prl:<20}"]

  for i in dcc.keys():
    id.append(f"|{i}")

  for a in dcc.values():
    nom_usuari.append(f"|{a[0]}")
    contrasenya.append(f"|{a[1]}")
    edat.append(f"|{a[2]}")
    privilg.append(f"|{a[3]}")

  with open("Usuaris.txt", "w") as f:
    f.write("".join(id))
    f.write("\n")
    f.write("".join(nom_usuari))
    f.write("\n")
    f.write("".join(contrasenya))
    f.write("\n")
    f.write("".join(edat))
    f.write("\n")
    f.write("".join(privilg))


def afegir(dcc, nom_usuari, contrasenya, edat = "0", privilg = "No"):

  id = len(dcc)
  nom_usuari = f"{nom_usuari:<20}"

  llista_noms = []
  for i in dcc.values():
    llista_noms.append(i[0])

  if nom_usuari not in llista_noms:
    dcc[f"{id:<20}"] = [nom_usuari, f"{contrasenya:<20}", f"{edat:<20}", f"{privilg:<20}"]

  else:
    print("Aquest nom d'usuari ja està ocupat")

  return(dcc)


#Programa principal

try:

  mode = sys.argv[1]

  if mode == "afegir":
    diccionari = file2dic()
    nom_usuari = sys.argv[2]
    contrasenya = sys.argv[3]

    try:
      edat = sys.argv[4]

      try: 
        privilg = sys.argv[5]
        diccionari = afegir(diccionari, nom_usuari, contrasenya, edat, privilg)

      except IndexError:
        diccionari = afegir(diccionari, nom_usuari, contrasenya, edat)

    except IndexError:
      diccionari = afegir(diccionari, nom_usuari, contrasenya)

    dic2file(diccionari)

except FileNotFoundError:
  id_prl = "Id"
  nom_usuari_prl = "Nom_Usuari"
  contrasenya_prl = "Contrasenya"
  edat_prl = "Edat"
  privilg_prl = "Privilegi"

  with open("Usuaris.txt", "w") as f:
    f.write(f"{id_prl:<20}|{0:<20}")
    f.write("\n")
    f.write(f"{nom_usuari_prl:<20}|{0:<20}")
    f.write("\n")
    f.write(f"{contrasenya_prl:<20}|{0:<20}")
    f.write("\n")
    f.write(f"{edat_prl:<20}|{0:<20}")
    f.write("\n")
    f.write(f"{privilg_prl:<20}|{0:<20}")
