#Funció recursiva

def binary(llista, n, pos = 0):

  if len(llista) < 2**n:

    a = llista[pos]

    for i in range(n):
      aux = []

      for item in a:
        aux.append(item)

      aux[i] = "1"
      b = ""
      b = b.join(aux)

      if b not in llista:
        llista.append(b)

    return binary(llista, n, pos+1)

  else:

    llista_final = []

    if n % 2 == 0:

      for i in llista:

        meitat1 = i[n//2:] 
        sum1 = 0
        for num in meitat1:
          sum1 += int(num)

        meitat2 = i[:n//2] 
        sum2 = 0
        for num in meitat2:
          sum2 += int(num)

        if sum1 == sum2:

          llista_final.append(i)

    else:

      for i in llista:

        meitat1 = i[n//2 + 1:] 
        sum1 = 0
        for num in meitat1:
          sum1 += int(num)

        meitat2 = i[:n//2] 
        sum2 = 0
        for num in meitat2:
          sum2 += int(num)

        if sum1 == sum2:

          llista_final.append(i)

    return llista_final


#Programa principal

n = int(input("Introdueix el nombre de dígits per a la cerca: "))
zeros = n*"0"
llista = [zeros]

print(binary(llista, n))
