#!/usr/bin/python3
#-*- coding: utf-8 -*-

import random

dict = {}

try:

	with open("word_dict.txt", "r") as f:
		#Convertim cada línia del fitxer en una parella paraula - definició
		lines = f.readlines()
		
		for i in lines:
			line_split = i.strip().split()
			paraula = line_split[0][:-1]
			definicio = " ".join(line_split[1:])
			
			#Afegim cada parella paraula - definició al diccionari
			
			dict[paraula] = definicio
			
	opt = input(">>> ")		
			
	while opt.lower() != "exit dictionary":
		
		if opt.lower() == "editar":
			paraula = input("Quina paraula vols afegir/ediar? ")
			
			if paraula in dict.keys():
				edit = input("Aquesta paraula ja existeix en el diccionari, vols editar la seva definició? ")			
				if edit.lower() == "si":
					definicio = input("Escriu la definició del vocable: ")
					
				elif edit.lower() == "no":
					definicio = dict.get(paraula)
					
				else:
					print("Respon solament amb si / no")
					definicio = dict.get(paraula)
					
			else:
				definicio = input("Escriu la definició del vocable: ")
				
			print("\n")
				
			#Creem i ordenem la nova llista de paraules
			
			claus = list(dict.keys())
			
			'''
			pos = 0
			while claus[pos] < paraula:
			'''	
				
			claus.append(paraula)
			claus.sort()

			#Actualitzem el diccionari, amb els elements ordenats
			
			new_dict = {}
			
			for element in claus:
				if element != paraula:
					new_dict[element] = dict.get(element)
				
				else:
					new_dict[element] = definicio
					
			dict = new_dict
			
		elif opt.lower() == "visualitzar":
			print("\n")
			for key in dict.keys():
				
				frase = f"{key}: {dict.get(key)}\n"
				print(frase)
				
		elif opt.lower() == "buscar":
			paraula = input("Quina paraula vols cercar? ")
			
			if paraula in dict.keys():				
				print(f"Definició de {paraula}: {dict.get(paraula)}\n")
				
			else:
				print("Aquesta paraula no es troba al diccionari\n ")
				
		elif opt.lower() == "esborrar diccionari":		
			seguretat = input("Estàs segur/a de que vols esborrar el diccionari? ")
			
			if seguretat.lower() == "si":				
				num_ordinador = random.randint(1,100)
				print(num_ordinador)
				num_usuari = input("Escriu el número que veus per pantalla: ")
				
				if str(num_ordinador) == num_usuari:
					print("--- Esborrant diccionari ---")
					dict.clear()
					
		else: 
			print("L'ordre introduïda no existeix\n ")
			
		opt = input(">>> ")	
		
	with open("word_dict.txt", "w") as f:
		
		for key in dict.keys():
			frase = f"{key}: {dict.get(key)}\n"
			f.write(frase)
			
except FileNotFoundError:
	f = open("word_dict.txt", "w")
	print("S'ha creat el fitxer 'word_dict.txt' on s'emmagatzemaran els mots i les seves definicions")
	
		
