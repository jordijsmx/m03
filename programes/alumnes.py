from sys import argv as entrada
import gestio_llista

dadesAlumnes = []

for i in range(int(entrada[1])):
	i += 1
	alumne = []
	print(f"Dades alumne {i}")
	dni = input("Introdueix un DNI: ")
	nom = input("Introdueix un nom: ")
	cognom = input("Introdueix un cognom: ")
	print("\n")
	
	alumne.append(dni)
	alumne.append(nom)
	alumne.append(cognom)
	
	dadesAlumnes.append(alumne)
	
# A partir d'aquí podríem utilitzar el mòdul gestio_llista.py per tal de 
# visualitzar les dades de la llista dadesAlumnes tal com l'usuari ho indiqui

# Per exemple:

print("\nÚltim alumne introduït:\n")
gestio_llista.get_alumne_n(dadesAlumnes, -1)

print("\n\nAlumnes ordenats en base al dni (ordre creixent):\n")
gestio_llista.get_alumnes_dni(dadesAlumnes)

print("\n\nAlumnes ordenats en base al nom (ordre creixent):\n")
gestio_llista.get_alumnes_nom(dadesAlumnes)
