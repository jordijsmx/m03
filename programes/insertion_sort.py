llista = [1,3,2,0,9,7,-4,100,8,7,0,1,2,3,-4]

for i in range(1,len(llista)):
  key = llista[i]
  continuar = True
  a = i-1
  while a >= 0 and continuar:
    if key < llista[a]:
      llista[a+1] = llista[a]
      llista[a] = key
      a -= 1
    else: 
      continuar = False

print(llista)
