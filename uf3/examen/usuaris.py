#!/usr/bin/python3
#-*- coding: utf-8 -*-

import sys

def file2dic():
    '''
    Desa la informació de cada usuari al diccionari.
    Input: None
    Output: diccionari
    '''
    diccionari = afegir(diccionari)

    try:    
        with open("usuaris.txt","r") as fitxer:
            lines = fitxer.readlines()

            for i in lines:

                numero = i[0]
                valors = i[1]

                diccionari[numero] = [valors]
    
    except FileNotFoundError:
        open("usuaris.txt","w")

    return diccionari

      
def dic2file(diccionari):
    '''
    Imprimeix el diccionari al fitxer escollit.
    Input: diccionari
    Output: None
    '''
    with open("usuaris.txt","w") as fitxer:
        for i in diccionari:
            fitxer.write(f"{i}:{diccionari[i]}")

    return None

def afegir(diccionari,nom_usuari,contrasenya,edat=0,privilegis="No"):
    '''
    Desa nou usuari al diccionari.
    Input: diccionari, nom (cadena), contrasenya (cadena), edat (int) OPCIONAL, privilegis (cadena) OPCIONAL
    Output: diccionari
    '''
    numero = 1
    nom_usuari = sys.argv[2]
    contrasenya = sys.argv[3]

    if nom_usuari in diccionari:
        print("Aquest nom d'usuari està ocupat.")
    else:
        diccionari.update({numero:[nom_usuari,contrasenya,edat,privilegis]})
    
    return diccionari

# Codi principal

dicc = {}

id = 1
nom = sys.argv[2]
contrasenya = sys.argv[3]

try:    
    with open("usuaris.txt","r") as fitxer:
        if sys.argv[1] == "afegir":
            noms = []
            passwd = []
            edats = []
            admin = []

            lines = fitxer.readlines()

            for i in lines:
                noms.append(nom)
                passwd.append(contrasenya)

        print(dicc)
except FileNotFoundError:
        open("usuaris.txt","w")