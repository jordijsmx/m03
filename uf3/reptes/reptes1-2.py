from sys import argv as arg

# Repte 1 
# Programa que crea un fitxer de text buït
'''
open("fitxer.txt","w")
'''
# Repte 2
# Programa que crea fitxer de text, es guardi amb el nom corresponent al primer
# argument i que contingui el que posem al segon argument.
f = open(arg[1],"w")
f.write(arg[2])