import os

fitxer = input("Introdueix el nom del fitxer: ")

paraula = ''
ruta = ''

while fitxer != "sortir" and paraula != 'sortir' and ruta != 'sortir':
    try:
        with open(fitxer, "r") as f:
            linies = f.readlines()

            if paraula == '':
                paraula = input("Introdueix la paraula a buscar: ")
            num=0
            nova_llista = []
            valor = True
           
            for i in linies:
                if paraula in i:
                    num+=1
                    nova_llista.append(i)
                    valor = False
            
            print(f"La paraula apareix {num} vegades.")

            if valor:
                paraula = input("Error: la paraula no existeix. Escriu 'sortir' o escriu una altra paraula a buscar: ")

            else:
                print("Es crearà un nou fitxer amb les línees amb la paraula trobada.")

                while ruta != 'sortir':
                    if ruta == '':
                        ruta = input("Introdueix la ruta absoluta a on desar el nou fitxer: ")

                    if os.path.isdir(ruta):
                        nom = input("Introdueix el nom del fitxer: ")
                        with open(f"{ruta}/{nom}","w") as fitxer_nou:
                            for i in nova_llista:
                                fitxer_nou.write(i)
                                ruta = "sortir"

                            print("El fitxer s'ha creat amb èxit.")
                            
                        with open(f"{ruta}/{nom}","r") as fitxer_nou:
                            print(f.read(fitxer_nou))
                            ruta = "sortir"                        
                        
                    else:
                        ruta = input("Error: no és una ruta vàlida.\nEscriu 'sortir' o una ruta vàlida: ")

    except IsADirectoryError:
        fitxer = input("Error: has introduït un directori.\nEscriu 'sortir' o el nom del fitxer correctament: ")

    except FileNotFoundError:
        fitxer = input("Error: el fitxer no existeix.\nEscriu 'sortir' o el nom del fitxer correctament: ")

    except UnicodeDecodeError:
        fitxer = input("Error: el fitxer no és llegible.\nEscriu 'sortir' o el nom d'un fitxer llegible: ")