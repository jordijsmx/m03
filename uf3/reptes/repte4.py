# Repte 4
'''
Desenvolupeu un programa on s’hi introdueixi el nom d’un fitxer, amb el seu
directori complet corresponent. Si el fitxer no existeix, el programa avisa a l’usuari que el
fitxer no existeix o no es troba al directori introduït. Si el fitxer existeix, el programa copia
tot el contingut d’aquest en un nou fitxer anomenat com l’original però situat en un
directori un nivell més aprop al directory root. Si el fitxer original ja es troba al directori
root, el programa no fa res.
'''
from sys import argv as arg

try:
    with open(arg[1], "r") as f:
        linies = f.readlines()

    ruta = arg[1]
    aux = ruta.split("/")
    aux.pop(-2)

    if len(aux) != 1:
        nova_ruta = ("/").join(aux)
    
    print(nova_ruta)
      
    with open(nova_ruta,"w") as ftx:
        for i in linies:
            ftx.write(i)

except FileNotFoundError:
    print("Error: el fitxer no existeix o no es troba al directori introduït.")

'''
with open("text.txt","a") as f:   # a és AFEGIR
    f.write("asdadas")
'''