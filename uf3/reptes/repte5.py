'''
Repte 5
'''
import sys

try:
    with open(sys.argv[1],"r") as fitxer:
        noms= []
        passwd = []
        edats = []
        admin = []
        
        linies = fitxer.readlines()

        for i in linies:
            llista = i.split(",")

            noms.append(llista[0])
            passwd.append(llista[1])
            edats.append(llista[2])
            admin.append(llista[3].strip())  # strip per eliminar salts de línia
      
    with open("Llista_usuaris.txt","w") as fitxer_usuaris:
        fitxer_usuaris.write("Noms: " + (", ").join(noms) + "\n")
        fitxer_usuaris.write("Contrasenyes: " + (", ").join(passwd) + "\n")
        fitxer_usuaris.write("Edats: " + (", ").join(edats) + "\n")
        fitxer_usuaris.write("Administradors: " + (", ").join(admin) + "\n")

except FileNotFoundError:
    print("Error: el fitxer no existeix o no es troba al directori introduït.")