from sys import argv as arg

# Repte 3
# Determina si el fitxer existeix en un directori o no. Si existeix, llegir les 5
# primeres linees. Si no existeix, demanar a l'usuari un nom de fitxer i crear-lo al
# directori corresponent

try: 
    with open(arg[1],"r") as fitxer:
        num_files=5
    #    fitxer = open(arg[1],"r")

        for i in range(0,num_files):
            print(fitxer.readline().strip())

except FileNotFoundError:
    f = input("Introdueix un nom de fitxer: ")

    open(f,"w")

'''
REPTE 2
try:
    with open(arg[1],"r") as f:
        a = f.readlines()
    
    opcio = input("El fitxer ja existeix, el vols substituir?: ")
    if opcio == "si":
        f = open(arg[1], "w")
    
except FileNotFoundError:
    f = open(arg[1],"w")
'''