from sys import argv as arg

try:
    fitxer = arg[1]

    if len(arg) == 3:
        num_files = int(arg[2])

    else:
        num_files = 5

except:
    try:
        fitxer = arg[2]

        if len(arg) == 3:
            num_files = int(arg[1])

        else:
            num_files = 5

    except:
        print("Error: no s'ha especificat correctament la visualització de files.")
    
    else:
        try:
            with  open(fitxer,"r") as f:
                lines = f.readlines()
                lines_tail = lines[-num_files:]

            for i in lines_tail:
                    print(i.strip())
        except:
            print("Error: problema d'accés o lectura al fitxer.")

else: # al'else s'entrà només quan NO ES TROBEN EXCEPTS.
    try:
        with  open(fitxer,"r") as f:
            lines = f.readlines()
            lines_tail = lines[-num_files:]

        for i in lines_tail:
                print(i.strip())
    except:
        print("Error: problema d'accés o lectura al fitxer.")