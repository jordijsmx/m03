# Error info:
# Error [num]: [error]
# exemple: Error 3: FileNotFoundError
# Error 24: UnicodeDecodeError <-- Si posem aixo aqui, vol dir q ho gestionarem al prog amb except [tipus]
# i una explicació del que ha passat i, amés "sys.exit(n)"

from sys import argv as arg
import sys

try:
    with open(arg[1], "r") as f:
        linies = f.readlines()

    ruta = arg[1]
    aux = ruta.split("/")
    aux.pop(-2)

    if len(aux) != 1:
        nova_ruta = ("/").join(aux)

        with open(nova_ruta,"w") as ftx:
            for i in linies:
                ftx.write(i)

except FileNotFoundError: # except tipus error
    print("Error: el fitxer no existeix o no es troba al directori introduït.") # indica error
    sys.exit(13) # acaba el programa i treu missatge per stderr, no executa res mes
                 # Si posem un número, al fer echo $? indicara el nombre                
