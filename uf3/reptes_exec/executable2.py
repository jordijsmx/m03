#!/usr/bin/python3
#-*- coding: utf-8 -*-

'''
. Llegeix un fitxer i en mostra les n primeres files. Hem de poder entrar el nom
del fitxer com a primer argument i el número de files com a segon, de la mateixa manera que
el programa ha de funcionar correctament si introduïm el nombre de files com a primer
argument i el nom del fitxer com a segon. Si no s’indica el número de files que es vol
visualitzar, el programa visualitza les primeres 10 files del fitxer per defecte. Què passa si el
fitxer té menys línies que les que l’usuari demana visualitzar?
'''

import sys

if len(sys.argv) == 3:
    if sys.argv[1].isdigit():
        nom = sys.argv[2]
        num = int(sys.argv[1])
    else:
        nom = sys.argv[1]
        num = int(sys.argv[2])
else:
    nom = sys.argv[1]
    num = 10

try:
    with open(nom,"r") as fitxer:
        contingut = fitxer.readlines()

        if num > len(contingut):
            num = len(contingut)

        for i in range(num):
            print(contingut[i].strip())
        
except FileNotFoundError:
    print(f"No s'ha pogut trobar el fitxer {nom}")