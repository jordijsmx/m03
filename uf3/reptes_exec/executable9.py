#!/usr/bin/python3
#-*- coding: utf-8 -*-
'''
Fes un programa que tingui la funcionalitat de l’executable 7 i de l’executable
8 a la vegada. Aquest programa permet fer una còpia d’un fitxer original mitjançant un
filtratge per columnes, indicant quin camp ens interessa (funcionalitat de l’executable 7) i un
filtratge per files, indicant quina és la paraula clau que defineix les files a copiar (funcionalitat
de l’executable 8).
'''

import sys

fitxer = sys.argv[1]
paraula = sys.argv[2]
nom_fitxer = sys.argv[3]
camp = int(sys.argv[4])

with open(fitxer,'r') as f:
    lines = f.readlines()
    apareix = []
    for i in lines:
        linia_list = i.split(':')
        if len(linia_list) < camp:
            camp= len(linia_list)
        if paraula in i:
            apareix.append(linia_list[int(camp)-1].strip())


ubicacio= fitxer.split('/')[:-1]
direccio = '/'.join(ubicacio)

with open(f'{direccio}/{nom_fitxer}','w') as f:
    for i in apareix:
        f.write(f'{i}\n')