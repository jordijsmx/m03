#!/usr/bin/python3
#-*- coding: utf-8 -*-

import sys

try:
    with open(sys.argv[1],"r") as fitxer:
        contingut = fitxer.read()
        print(contingut)
        
except FileNotFoundError:
    print(f"No s'ha pogut trobar el fitxer {sys.argv[1]}")