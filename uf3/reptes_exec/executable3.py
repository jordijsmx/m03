#!/usr/bin/python3
#-*- coding: utf-8 -*-

import sys

if len(sys.argv) == 3:
    if sys.argv[1].isdigit():
        nom = sys.argv[2]
        num = int(sys.argv[1])
    else:
        nom = sys.argv[1]
        num = int(sys.argv[2])
else:
    nom = sys.argv[1]
    num = 10

try:
    with open(nom,"r") as fitxer:
        contingut = fitxer.readlines()
        tail = contingut[-num:]
            
        for i in tail:
            print(i.strip())
        
except FileNotFoundError:
    print(f"No s'ha pogut trobar el fitxer {nom}")