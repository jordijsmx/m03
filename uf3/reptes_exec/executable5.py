#!/usr/bin/python3
#-*- coding: utf-8 -*-
'''
executable 5

El programa següent llegeix un fitxer i copia el fitxer en la direcció indicada
per l’usuari. L’usuari introdueix el fitxer com a primer argument i la direcció com a segon.
Què passa si el directori que introdueix l’usuari no existeix? I si no es tenen permisos per
accedir o actuar sobre directori indicat?
'''
import sys


try: 
    with open(sys.argv[1],"r") as fitxer:
        lines = fitxer.readlines()
    
    fitxer = sys.argv[1].split("/")

    
    if sys.argv[2][-1] == "/":
        nou_fitxer = sys.argv[2] + fitxer[-1]

    else:
        nou_fitxer = sys.argv[2] + "/" + fitxer[-1]

    with open(nou_fitxer,"w") as fitxer:
        for i in lines:
            fitxer.write(i)

except PermissionError:
    print("No tens els permisos necessaris per accedir al directori destí.")