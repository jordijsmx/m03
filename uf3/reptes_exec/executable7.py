'''
El programa següent llegeix un fitxer amb diverses línies, on cada línia té
varis camps separats entre ells per dos punts “ : ” . Seguidament el fitxer mostra per pantalla
totes les línies del fitxer que s’ha llegit però mostrant solament un camp en concret. L’usuari
indica el camp que vol visualitzar mitjançant un número que fa referència al camp
corresponent. El primer camp correspon al nombre 1 i l’últim camp correspon al nombre n
(on n és el nombre de camps que té cada línia). L’usuari introdueix com a primer argument el
fitxer i com a segon argument el nombre corresponent al camp que vol visualitzar.
Què passa si el fitxer no té varis camps a les seves línies o sí que en té però aquests no
estan separats per dos punts “ : “ ?
Què passa si l’usuari indica que vol visualitzar un camp superior al nombre de camps que
tenen les línies del fitxer?
Què passa si no totes les línies del fitxer introduït tenen el mateix nombre de camps?
Per exemple, el fitxer passwd té línies com la següent: root:x:0:0:root:/root:/bin/bash
Les línies d’aquest fitxer, com podem observar tenen 7 camps separats entre ells per dos
punts “ : “ . Si l’usuari introdueix el fitxer passwd i indica que vol visualitzar el camp número
5, es mostra el següent per pantalla:
root
daemon
bin
sys
… (continua fins al final del fitxer
'''
import sys

try:
    with open(sys.argv[1],"r") as fitxer:
        lines = fitxer.readlines()

    camp = int(sys.argv[2]) - 1
    error_separador = True
    error_rang = True
 
    for i in lines:
        if ":" not in i:
            if error_separador:
                print("Error: els camps no estan separats per ':'")
                error_separador = False
        else:
            i = i.split(":")
    
            if camp >= 0 and camp < len(i):
                print(i[camp])
            else:
                if error_rang:
                    print("Error: L'index del camp és fora de rang.")
                    error_rang = False
        
except FileNotFoundError:
    print("No s'ha trobat el fitxer especificat.")