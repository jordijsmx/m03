#!/usr/bin/python3
#-*- coding: utf-8 -*-

import sys

try:
    with open(sys.argv[1],"r") as fitxer:
        num = 1
        for i in fitxer:
            print(f"Línia {num}: {len(i.strip())}")
            num += 1
except FileNotFoundError:
    print(f"No s'ha pogut trobar el fitxer {sys.argv[1]}")