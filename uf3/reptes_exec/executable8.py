'''
El programa següent llegeix un fitxer amb diverses línies, el qual s’indica com
a primer argument, i una paraula o conjunt de caràcters que s’introdueix com a segon
argument. Seguidament, el programa guarda en la mateixa direcció on es troba el fitxer que
s’ha llegit, una còpia d’aquest format solament per les files on, en el fitxer original, hi apareix
la paraula o el conjunt de caràcters indicat. El fitxer nou es guarda amb el nom que s’indiqui
com a tercer argument.
Què passa si el conjunt de caràcters indicat conté un o varis espais entre els seus caràcters?
'''
import sys

fitxer = sys.argv[1]
paraula = sys.argv[2]
nom_fitxer = sys.argv[3]

with open(fitxer,'r') as f:
    lines = f.readlines()
    apareix = []
    for i in lines:
        if paraula in i:
            apareix.append(i)


ubicacio= fitxer.split('/')[:-1]
direccio = '/'.join(ubicacio)

with open(f'{direccio}/{nom_fitxer}','w') as f:
    for i in apareix:
        f.write(i)