'''
. El programa següent llegeix un fitxer i el copia en la direcció indicada per
l’usuari, de la mateixa manera que ho fa l’executable 5. Ara bé, el fitxer que el programa
copia i guarda, és el mateix que el fitxer original però amb tots els caràcters escrits en lletra
majúscula. L’usuari introdueix el fitxer com a primer argument i la direcció com a segon.
'''
import sys


try: 
    with open(sys.argv[1],"r") as fitxer:
        lines = fitxer.readlines()
    
    fitxer = sys.argv[1].split("/")

    
    if sys.argv[2][-1] == "/":
        nou_fitxer = sys.argv[2] + fitxer[-1]

    else:
        nou_fitxer = sys.argv[2] + "/" + fitxer[-1]

    with open(nou_fitxer,"w") as fitxer:
        for i in lines:
            fitxer.write(i.upper())

except PermissionError:
    print("No tens els permisos necessaris per accedir al directori destí.")