import sys

try:
    with open(sys.argv[1],"a+") as fitxer:
        fitxer.seek(0)
        linies = fitxer.readlines()
        num = 1
        for i in linies:
            num+=1
        
        fitxer.write(f"\nAquest fitxer té {num} línees.")

except FileNotFoundError:
    print("Error: el fitxer no existeix o no es troba al directori introduït.")

''' versio profe
import sys

with open(sys.argv[1], "a+") as f:
	f.seek(0)
	a = f.readlines()
    if len(linies) != 0 and linies != ["\n"]:
        fitxer.write("\n")
        fitxer.write(f"\nAquest fitxer té {len(linies)} línees.")
        
    else:
        with open(sys.argv[1],"w") as fitxer:
            fitxer.write(f"\nAquest fitxer té 0 línees.")
'''