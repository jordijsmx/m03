#!/usr/bin/python3
#-*- coding: utf-8 -*-

import random

try:
    with open("word_dict.txt","r") as fitxer:
        diccionari = {}
        execucio = True

        lines = fitxer.readlines()

        for i in lines:
            line_split = i.strip().split()
            paraula = line_split[0][:-1]
            definicio = (" ").join(line_split[1:])

            diccionari[paraula] = definicio

        while execucio:
            ordre = input("\n>>> ")

            if ordre.lower() == "visualitzar":
                for i in sorted(diccionari):
                    print(f"\n{i}: {diccionari[i]}")

            elif ordre.lower() == "editar":
                paraula = input("\nQuina paraula vols afegir/editar? ")
                if paraula in diccionari.keys():
                    editar = input("Aquesta paraula ja existeix en el diccionari, vols editar la seva definició? ")
                    if editar.upper() == "SI":
                        voc = input("Escriu la definició del vocable: ")
                        diccionari[paraula] = voc
                    elif editar.upper() == "NO":
                        ordre = ''
                        voc = diccionari.get(paraula)
                    else:
                        print("Respon solament amb si/no.")
                        voc = diccionari.get(paraula)
                else:
                    voc = input("Escriu la definició del vocable: ")
                    diccionari.update({paraula:voc})
             
            elif ordre.lower() == "buscar":
                buscar = input("\nQuina paraula vols cercar? ")
                if buscar in diccionari.keys():
                    valor = diccionari[buscar]
                    print(f"\nDefinició de {buscar}: {valor}")
                else:
                    print(f"\nLa paraula {buscar} no es troba al diccionari.")

            elif ordre.lower() == "esborrar diccionari":
                esborrar = input("\nS'esborrara el diccionari, estàs segur? ")
                if esborrar.upper() == "SI":
                    num = random.randint(1,100)
                    print(num)
                    num2 = int(input("Escriu el número que veus per pantalla: "))
                    if num == num2:
                        print("---Esborrant diccionari---")
                        diccionari = {}
                        diccionari.clear()
                elif esborrar.upper() == "NO":
                    ordre = ''
                else:
                    print("Respon solament amb si/no.")

            elif ordre.lower() == "exit dictionary":
                with open("word_dict.txt","w") as fitxer:
                    for i in sorted(diccionari):
                        fitxer.write(f"{i}: {diccionari[i]}\n")
                execucio = False
            else:
                print("\nError: Aquesta ordre no existeix.\n")
                print("Ordres disponibles:")
                print("- visualitzar\n- editar\n- buscar\n- esborrar diccionari\n- exit dictionary")
           
except FileNotFoundError:
    print("S'ha creat el fitxer: 'word_dict.txt', on s'emmagatzemaran els mots i les seves definicions.")
    open("word_dict.txt","w")