from sys import argv as arg
import sys

try:
    with open(arg[1], "r") as f:
        linies = f.readlines()

    ruta = arg[1]
    aux = ruta.split("/")
    aux.pop(-2)

    if len(aux) != 1:
        nova_ruta = ("/").join(aux)

        with open(nova_ruta,"w") as ftx:
            for i in linies:
                ftx.write(i)

except FileNotFoundError: 
    print("Error: el fitxer no existeix o no es troba al directori introduït.")
    sys.exit(13)